# About

This folder contains the information required to build the doxygen documentation of MAiNGO.
You can build the documentation for you version of MAiNGO by running doxygen on the Doxyfile in this folder.
The ready-to-read documentation for the most recent version of MAiNGO can also be found [here](https://avt-svt.pages.rwth-aachen.de/public/maingo).