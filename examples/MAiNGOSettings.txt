#Comments are written with #


#----------------------------------------------------Tolerances & termination--------------------------------------------------

#absolute optimality tolerance, has to be >=1e-9    (default: 1e-2)
#epsilonA                                1e-2

#relative optimality tolerance, has to be >=1e-9    (default: 1e-2)
#epsilonR                                1e-2

#inequality tolerance, has to be >=1e-9    (default: 1e-6)
#deltaIneq                              1e-6

#equality tolerance, has to be >=1e-9    (default: 1e-6)
#deltaEq                                1e-6

#minimum relative acceptable size of node, nodes smaller than this threshold will be treated as one point,
#defined as max(1e-12,min(relNodeTol,deltaIneq*1e-1,deltaEq*1e-1)), has to be >=1e-12    (default: min(1e-9,deltaIneq*1e-1,deltaEq*1e-1))
#relNodeTol                             1e-9

#maximum number of nodes in memory in B&B, has to be >= 0 or -1 (=4294967295)   (default: maximum value for an object of type unsigned int: 4294967295)
#BAB_maxNodes                           -1

#maximum number of iterations in B&B, has to be >= 0 or -1 (=4294967295)   (default: maximum value for an object of type unsigned int: 4294967295)
#BAB_maxIterations                      -1

#maximum time, has to be >= 10 or -1 (=4294967295)   (default: 86400 = 24h)
#maxTime                                86400

#whether to ask the user before terminating because of time, node, or iteration limits, has to be 0 for FALSE or 1 for TRUE   (default: 0)
#confirmTermination                     0

#whether to terminate as soon as the first feasible point was found (no guarantee of global or local optimality!), has to be 0 for FALSE or 1 for TRUE   (default: 0)
#terminateOnFeasiblePoint               0

#target value for the lower bound on the optimal objective. MAiNGO terminates once LBD>=targetLowerBound - no guarantee of global or local optimality!  (default: ~1.8e308)
#targetLowerBound                       1e51

#target value for the upper bound on the optimal objective. MAiNGO terminates once UBD<=targetUpperBound - no guarantee of global or local optimality!  (default: ~-1.8e308)
#targetUpperBound                       -1e51


#------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------Pre-processing-------------------------------------------------------

#maximum number of local searches in root node - multistart    (default: 3)
#PRE_maxLocalSearches                   3

#maximum number of LPs solved in root node - range reduction LPs    (default: 10)
#PRE_obbtMaxRounds                      10

#whether to perform a multistart only, a B&B tree will not be constructed and no lower bounding problems will be solved, has to be 0 for FALSE and 1 for TRUE    (default: 0)
#PRE_pureMultistart                     0


#------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------B&B branching settings-------------------------------------------------

#node selection options:  NS_BESTBOUND = 0,  NS_DEPTHFIRST = 1,  NS_BREADTHFIRST = 2    (default: 0)
#BAB_nodeSelection                      0

#branching heuristic:   BV_ABSDIAM = 0,  BV_RELDIAM = 1    (default: 1)
#BAB_branchVariable                     1


#------------------------------------------------------B&B heuristics settings-------------------------------------------------

#range reduction in every node, has to be 0 for FALSE and 1 for TRUE    (default: 1)
#BAB_alwaysSolveObbt                    1

#coefficient to determine how fast the probability of doing OBBT decays with increasing depth of a given node in the B&B tree, has to be >=0 (default: 0)
#BAB_obbtDecayCoefficient               0

#do duality based bound tightening rounds, has to be 0 for FALSE and 1 for TRUE    (default: 1)
#BAB_dbbt                               1

#do probing, has to be 0 for FALSE and 1 for TRUE    (default: 0)
#BAB_probing                            0

#do constraint propagation, has to be 0 for FALSE and 1 for TRUE    (default: 1)
#BAB_constraintPropagation              1


#------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------LBP settings--------------------------------------------------------

#This setting also sets the solver for purely (mixed-integer) quadratic/linear problems.
#LBP solver: LBP_SOLVER_MAiNGO = 0, LBP_SOLVER_INTERVAL = 1, LBP_SOLVER_CPLEX = 2, LBP_SOLVER_CLP = 3    (default: 2 if CPLEX is available, otherwise 3)
#LBP_solver                             2

#LBP linearization points strategy: LINP_MID = 0, LINP_INCUMBENT = 1, LINP_KELLEY = 2,
#                                   LINP_SIMPLEX = 3, LINP_RANDOM = 4, LINP_SIMPLEX_KELLEX =5    (default: 0)
#LBP_linPoints                          0

#LBP_subgradient_intervals: whether to use the DAG interval subgradient heuristic or not, has to be 0 for FALSE and 1 for TRUE  (default: 1)
#LBP_subgradientIntervals               1

#how much improvement needs to be achievable (relative to initial diameter) to conduct OBBT at a node    (default: 0.01)
#LBP_obbtMinImprovement                 0.01

#number of consecutive iterations without LBD improvement needed to activate more aggressive scaling in LPs (e.g., CPLEX),
#has to be at least 100 and at most 100000   (default: 10000)
#LBP_activateMoreScaling                10000

#add auxiliary variables and equalities for factors occurring more that LBP_minFactorsForAux times, has to be 0 for FALSE and 1 for TRUE    (default: 0)
#LBP_addAuxiliaryVars                   0

#minimum number of occurences for a factor to be added as an auxiliary variable (if LBP_addAuxiliaryVars is TRUE)    (default: 2)
#LBP_minFactorsForAux                   2

#maximum number of added factors as auxiliaries (if LBP_addAuxiliaryVars is TRUE)    (default: 1)
#LBP_maxNumberOfAddedFactors            1


#------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------MC++ settings--------------------------------------------------------

#use multivariate McCormick, has to be 0 for FALSE and 1 for TRUE     (default: 1)
#MC_mvcompUse                           1

#multivariate computation tolerance, has to be in [1e-9,1e-12]    (default: 1e-9)
#MC_mvcompTol                           1e-9

#envelope computation tolerance, has to be in [1e-9,1e-12]    (default: 1e-9)
#MC_envelTol                            1e-9


#------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------UBP settings--------------------------------------------------------

#UBP solver to be used in pre-processing (i.e., multistart): UBP_SOLVER_EVAL = 0, UBP_SOLVER_COBYLA = 1, UBP_SOLVER_BOBYQA = 2, UBP_SOLVER_LBFGS = 3, UBP_SOLVER_SLSQP = 4,
#                                                            UBP_SOLVER_IPOPT = 5, UBP_SOLVER_KNITRO=6  (default: 5)
#UBP_solverPreprocessing                5

#maximum number of steps of UBP, has to be at least >=1   (default: 3000)
#UBP_maxStepsPreprocessing              3000

#maximum CPU time for UBP, has to be at least >=0.1   (default: 100)
#UBP_maxTimePreprocessing               100

#UBP solver to be used during Branch-and-Bound: UBP_SOLVER_EVAL = 0, UBP_SOLVER_COBYLA = 1, UBP_SOLVER_BOBYQA = 2, UBP_SOLVER_LBFGS = 3, UBP_SOLVER_SLSQP = 4,
#                                               UBP_SOLVER_IPOPT = 5, UBP_SOLVER_KNITRO=6  (default: 4)
#UBP_solverBab                          4

#maximum number of steps of UBP, has to be at least >=1   (default: 3)
#UBP_maxStepsBab                        3

#maximum CPU time for UBP, has to be at least >=0.1   (default: 10)
#UBP_maxTimeBab                         10

#whether to ignore current node bounds and solve the upper bounding problem over the original bounds only, has to be 0 for FALSE and 1 for TRUE   (default: 0)
#UBP_ignoreNodeBounds                   0


#------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------Epsilon-constraint settings------------------------------------------------

#number of points on the Pareto front to be computed via the epsilon-constraint method (only available via the C++ API)
#EC_nPoints                10


#------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------Output settings------------------------------------------------------

#output of B&B:  VERB_NONE = 0 (i.e., nothing at all),  VERB_NORMAL = 1 (i.e., regular progress reporting),   VERB_ALL = 2 (i.e., diagnostic information, very noisy!)    (default: 1)
#BAB_verbosity                          1

#output of LBP:  VERB_NONE = 0 (i.e., nothing at all),  VERB_NORMAL = 1 (i.e., only important warnings),   VERB_ALL = 2 (i.e., diagnostic information, very noisy!)    (default: 1)
#LBP_verbosity                          1

#output of UBP:  VERB_NONE = 0 (i.e., nothing at all),  VERB_NORMAL = 1 (i.e., only important warnings),   VERB_ALL = 2 (i.e., diagnostic information, very noisy!)    (default: 1)
#UBP_verbosity                          1

#print info every x number of nodes    (default: 100)
#BAB_printFreq                          100

#write into log file every x nodes    (default: 100)
#BAB_logFreq                            100

#whether to write to output stream (e.g., shell) and to log: LOGGING_NONE = 0, LOGGING_OUTSTREAM = 1, LOGGING_FILE = 2, LOGGING_FILE_AND_STREAM = 3    (default: 3)
#loggingDestination                     3

#write to log/csv file every x number of CPU seconds, works only if writeLog or writeCsv is activated, has to be >=10    (default: 1800)
#if set to -1, then a file containing all iterations of the B&B will be written at the end of optimization only
#writeToLogSec                          1800

#write a file MAiNGOresult.txt containing a summary about the best solution point found along with variable bounds, constraint residuals etc.
#the file will be only written if the problem has been solved successfully, has to be 0 for FALSE and 1 for TRUE    (default: 1)
#writeResultFile                           1

#write csv files with a summary of the B&B iterations as well as a summary of the solution and problem statistics, has to be 0 for FALSE and 1 for TRUE    (default: 0)
#writeCsv                               0

#write json file of the solution and problem statistics, has to be 0 for FALSE and 1 for TRUE    (default: 0)
#writeJson                              0

#whether to write output for every local search during multistart at the root node, has to be 0 for FALSE and 1 for TRUE    (default: 0)
#PRE_printEveryLocalSearch              0

#write to a different modeling language into the file "MAiNGO_written_model" before pre-processing
#min/max functions will be reformulated with abs(), hyperbolic functions such as tanh will be reformulated with exp(),
#bounding functions will be ignored, relaxation only constraints will be ignored
#LANG_NONE = 0, LANG_ALE = 1, LANG_GAMS = 2    (default: 0)
#modelWritingLanguage                   0


#-------------------------------------------------------------------------------------------------------------------------------
#---------------Settings for extension "B&B alorithm with growing datasets for large-scale parameter estimation"----------------

#which approach to use: GROW_APPR_DETERMINISTIC = 0, GROW_APPR_SSEHEURISTIC = 1, GROW_APPR_MSEHEURISTIC = 2    (default: 0)
#growing_approach                       0

#maximum CPU time in seconds for post-processing in heuristic approaches, has to be at least >=0   (default: 60)
#if set to 0, post-processing will be skipped
#growing_maxTimePostprocessing          60

#relative tolerance for considering dataset as the full dataset, has to be > 0 and < 1    (default: 0.9)
#growing_dataSizeTol                    0.9

#size of the smallest reduced dataset is x times size of full dataset, has to be > 0 and < 1     (default: 0.1)
#at least one data point will be used
#growing_dataSizeInit                   0.1

#whether to use resampling for the calculation of the lower bound based on the initial dataset, has to be 0 for FALSE and 1 for TRUE    (default: 0)
#growing_useResampling                  0

#size of new dataset to be added when augmenting relative to number of points of the full dataset    (default: 0.25)
#growing_augmentPercentage              0.25

#rule when to augment the dataset: AUG_RULE_CONST = 0, AUG_RULE_SCALING = 1, AUG_RULE_OOS = 2, AUG_RULE_COMBI = 3, AUG_RULE_TOL = 4, AUG_RULE_SCALCST = 5, AUG_RULE_OOSCST = 6, AUG_RULE_COMBICST = 7, AUG_RULE_TOLCST = 8    (default: 8)
#when using GROW_APPR_MSEHEURISTIC rules including SCAL(ING) cannot be used
#growing_augmentRule                    8

#augment dataset in nodes whose depth is a multiple of this freq    (default: 10)
#this setting will only be used when CONST (CST) is part of the augmentation rule
#growing_augmentFreq                    10

#weighting factor of heuristic lower bound, has to be > 0 and <= 1    (default: 1)
#this setting will only be used when SCAL(ING) is part of the augmentation rule
#growing_augmentWeight                  1

#absolute and relative tolerance on lower bound for augmenting, has to be > min(epsilonA,epsilonR)    (default: 1e-1)
#this setting will only be used when TOL is part of the augmentation rule
#growing_augmentTol                     1e-1


#-------------------------------------------------------------------------------------------------------------------------------

# ASCII MAiNGO
#                              /)_
#                             //\  `.
#                      ____,.//, \   \
#                 _.-´          `.`.  \
#               ,´                : `..\
#              :         ___       :
#             :       .´     `.     :
#            :         `.    /      ;
#           :           /   /      ;
#          :        __.´   /      :
#          ;      /       /      :
#          ;      `------´      /
#          :                   :
#           \                 /
#            `.             .`
#              '-._     _.-'
#                  `'''`