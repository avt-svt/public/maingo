# Example Problem Number 09 - Optimization with Multifidelity Gaussian processes embedded

First have a look at example '05_GaussianProcess', then come back here
Please copy the folder 'modelData' into the folder where your MAiNGO executable is in order to run the problem. If you are using Visual Studio, this should be the Release folder containing MAiNGO.exe.