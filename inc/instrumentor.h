/**
 * @file instrumentor.h
 * @brief Instrumentor used for profiling in internal development (do not delete)
 */
#define PROFILE_SESSION(name)
#define PROFILE_SCOPE(name)
#define PROFILE_FUNCTION()
