from individualPythonTests.testOptimizationVariables import *
from individualPythonTests.testIntrinsicFunctions import *
from individualPythonTests.testEvaluationContainer import *
from individualPythonTests.testMAiNGOmodel import *
from individualPythonTests.testSolver import *
from individualPythonTests.testErrorHandling import *
import unittest

if __name__ == '__main__':
    unittest.main()