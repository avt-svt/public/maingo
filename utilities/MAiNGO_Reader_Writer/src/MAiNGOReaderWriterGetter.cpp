/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include "MAiNGOReaderWriter.h"


using namespace maingo;
using namespace readerWriter;


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting total number of variables
unsigned int
MAiNGOReaderWriter::get_number_variables_GAMS_file()
{
    return _nvar;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// brief Function for getting number of continuous variables
unsigned int
MAiNGOReaderWriter::get_number_cont_variables_GAMS_file()
{
    return _ncontVar;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// brief Function for getting number of binary variables
unsigned int
MAiNGOReaderWriter::get_number_bin_variables_GAMS_file()
{
    return _nbinVar;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting number of integer variables
unsigned int
MAiNGOReaderWriter::get_number_int_variables_GAMS_file()
{
    return _nintVar;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting total number of constraints
unsigned int
MAiNGOReaderWriter::get_number_constraints_GAMS_file()
{
    return _ncons;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting number of equality constraints
unsigned int
MAiNGOReaderWriter::get_number_eq_constraints_GAMS_file()
{
    return _neq;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting number of >= inequality constraints
unsigned int
MAiNGOReaderWriter::get_number_gineq_constraints_GAMS_file()
{
    return _ngineq;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting number of <= inequality constraints
unsigned int
MAiNGOReaderWriter::get_number_lineq_constraints_GAMS_file()
{
    return _nlineq;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting number of fixed variables
unsigned int
MAiNGOReaderWriter::get_number_fixed_variables_GAMS_file()
{
    return _nfixedVar;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of the objective variable
unsigned int
MAiNGOReaderWriter::get_obj_variable_number_GAMS_file()
{
    return _objNr;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the name of the objective variable
std::string
MAiNGOReaderWriter::get_obj_variable_name_GAMS_file()
{
    return _objName;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting _minimizing
bool
MAiNGOReaderWriter::get_minimizing_GAMS_file()
{
    return _minimizing;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of variables read in from MAiNGO log
unsigned int
MAiNGOReaderWriter::get_number_variables_MAiNGO_log()
{
    return _nvarM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of equalities read in from MAiNGO log
unsigned int
MAiNGOReaderWriter::get_number_equalities_MAiNGO_log()
{
    return _neqM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of inequalities read in from MAiNGO log
unsigned int
MAiNGOReaderWriter::get_number_inequalities_MAiNGO_log()
{
    return _nineqM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of relaxation only equalities read in from MAiNGO log
unsigned int
MAiNGOReaderWriter::get_number_rel_only_eq_MAiNGO_log()
{
    return _neqRelaxationOnlyM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of relaxation only inequalities read in from MAiNGO log
unsigned int
MAiNGOReaderWriter::get_number_rel_only_ineq_MAiNGO_log()
{
    return _nineqRelaxationOnlyM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of solved ubd problems read in from MAiNGO log
unsigned int
MAiNGOReaderWriter::get_number_ubd_solved_MAiNGO_log()
{
    return _ubdProblemsSolvedM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of solved lbd problems read in from MAiNGO log
unsigned int
MAiNGOReaderWriter::get_number_lbd_solved_MAiNGO_log()
{
    return _lbdProblemsSolvedM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of iterations read in from MAiNGO log
unsigned int
MAiNGOReaderWriter::get_number_iterations_MAiNGO_log()
{
    return _niterationsM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of nodes in memory read in from MAiNGO log
unsigned int
MAiNGOReaderWriter::get_number_nodes_in_memory_MAiNGO_log()
{
    return _nnodesInMemoryM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of node where best solution was first found read in from MAiNGO log
unsigned int
MAiNGOReaderWriter::get_number_best_first_found_MAiNGO_log()
{
    return _bestSolFirstFoundM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of final lbd read in from MAiNGO log
double
MAiNGOReaderWriter::get_final_lbd_MAiNGO_log()
{
    return _finalLBDM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of final abs gap read in from MAiNGO log
double
MAiNGOReaderWriter::get_final_abs_gap_MAiNGO_log()
{
    return _finalAbsGapM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of final rel gap read in from MAiNGO log
double
MAiNGOReaderWriter::get_final_rel_gap_MAiNGO_log()
{
    return _finalRelGapM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of final obj value read in from MAiNGO log
double
MAiNGOReaderWriter::get_final_obj_val_MAiNGO_log()
{
    return _objValM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of CPU time needed read in from MAiNGO log
double
MAiNGOReaderWriter::get_cpu_time_needed_MAiNGO_log()
{
    return _cpuTimeNeededM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the number of wall time needed read in from MAiNGO log
double
MAiNGOReaderWriter::get_wall_time_needed_MAiNGO_log()
{
    return _wallTimeNeededM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the solution point read in from MAiNGO log
bool
MAiNGOReaderWriter::get_feasibility_MAiNGO_log()
{
    return _feasibleM;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the solution point read in from MAiNGO log
std::vector<std::pair<std::string, double>>
MAiNGOReaderWriter::get_solution_point()
{
    return _solutionPoint;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting additional output read in from MAiNGO log
std::vector<std::pair<std::string, double>>
MAiNGOReaderWriter::get_additional_output()
{
    return _additionalOutput;
}