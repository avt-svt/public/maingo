/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include "MAiNGOReaderWriter.h"

#include <algorithm>
#include <cmath>
#include <cstring>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <regex>
#include <sstream>

#ifdef GCC_FS_EXPERIMENTAL
#include <experimental/filesystem>
#else
#include <filesystem>
#endif


using namespace maingo;
using namespace readerWriter;


/////////////////////////////////////////////////////////////////////////////////////////////
// default constructor
MAiNGOReaderWriter::MAiNGOReaderWriter():
    _gamsFileName("gams.gms"), _nvar(0), _ncontVar(0), _nbinVar(0), _nintVar(0), _ncons(0), _neq(0), _nlineq(0), _ngineq(0), _nfixedVar(0), _objNr(0), _objName(""), _minimizing(true),
    _maingoFileName("bab.log"), _nvarM(0), _neqM(0), _nineqM(0), _neqRelaxationOnlyM(0), _nineqRelaxationOnlyM(0), _ubdProblemsSolvedM(0), _lbdProblemsSolvedM(0), _niterationsM(0),
    _nnodesInMemoryM(0), _bestSolFirstFoundM(0), _finalLBDM(0), _finalAbsGapM(0), _finalRelGapM(0), _objValM(0), _cpuTimeNeededM(0), _wallTimeNeededM(0), _feasibleM(0), _objSingle(true),
    _solutionPoint(std::vector<std::pair<std::string, double>>()), _additionalOutput(std::vector<std::pair<std::string, double>>())
{
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for reading in the GAMS convert file
void
MAiNGOReaderWriter::read_GAMS_convert_file(const std::string &gamsFileName)
{

    _gamsFileName = gamsFileName;
    // Clear all internal variables
    _file.clear();
    _nvar        = 0;
    _ncontVar    = 0;
    _nbinVar     = 0;
    _nintVar     = 0;
    _ncons       = 0;
    _neq         = 0;
    _nlineq      = 0;
    _ngineq      = 0;
    _nfixedVar   = 0;
    _objNr       = 0;
    _objName     = "";
    _objFunction = "";
    _objSingle   = true;
    _minimizing  = true;
    _contVariables.clear();
    _binVariables.clear();
    _intVariables.clear();
    _constraints.clear();
    _eqConstraints.clear();
    _gineqConstraints.clear();
    _lineqConstraints.clear();
    _initialPointCont.clear();
    _initialPointBin.clear();
    _initialPointInt.clear();

    _read_file_and_check_it_is_gams_convert();

    _extract_problem_info();

    _extract_variable_names();

    _extract_variable_bounds();

    _extract_initial_point();

    _extract_constraints();

    std::cout << "GAMS convert file " << _gamsFileName << " read successfully." << std::endl;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting total number of variables etc.
void
MAiNGOReaderWriter::_read_file_and_check_it_is_gams_convert()
{
    std::ifstream inFile;
    inFile.open(_gamsFileName);
    if (!inFile.is_open()) {
        throw std::runtime_error("Error: Could not open file " + _gamsFileName + ".");
    }

    std::string line;
    while (std::getline(inFile, line)) {
        const auto strEnd = line.find_last_not_of(" ");
        line              = line.substr(0, strEnd + 1);
        line              = std::regex_replace(line, std::regex("\\r"), "");
        _file.push_back(line);
    }

    inFile.close();

    if (_file.empty()) {
        throw std::runtime_error("Error: File " + _gamsFileName + " is empty.");
    }

    const std::size_t positionOfConvertString = _file[0].find("written by GAMS Convert");
    if (positionOfConvertString == std::string::npos) {
        throw std::runtime_error("Error: File " + _gamsFileName + " does not look like not a GAMS convert file.");
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting total number of variables etc.
void
MAiNGOReaderWriter::_extract_problem_info()
{
    bool foundEqNumbers = false;
    bool foundVarNumbers = false;
    bool foundFixedVarNumber = false;
    bool foundObjective = false;

    for (size_t counter = 0; counter < _file.size(); counter++){
        std::string line = _file[counter];

        // Consider only comment lines, since these contain the model info
        if(line[0] != '*') {
            continue;
        }

        // Number of equations
        if (line.find("Equation counts") != std::string::npos) {
            counter++;    // Line with Total,E,G,L,...
            counter++;    // Line holding the numbers
            line = _file[counter];
            std::istringstream iss(line);
            std::string asterisk;
            iss >> asterisk;
            iss >> _ncons;
            iss >> _neq;
            iss >> _ngineq;
            iss >> _nlineq;
            // Check for N,X,C,B equations
            unsigned int check;
            for (unsigned int i = 0; i < 4; i++) {
                iss >> check;
                if (check != 0) {
                    throw std::runtime_error("Error: File " + _gamsFileName + " contains non E,G,L equations.");
                }
            }
            foundEqNumbers = true;
            continue;
        }

        // Number of variables
        if (line.find("Variable counts") != std::string::npos) {
            counter++;    // Line with x,b,i,...
            counter++;    // Line with Total,cont,binary,...
            counter++;    // Line holding the numbers
            line = _file[counter];
            std::istringstream iss(line);
            std::string asterisk;
            iss >> asterisk;
            iss >> _nvar;
            iss >> _ncontVar;
            iss >> _nbinVar;
            iss >> _nintVar;
            // Check for sos1,sos2,scont,sint variables
            unsigned int check;
            for (unsigned int i = 0; i < 4; i++) {
                iss >> check;
                if (check != 0) {
                    throw std::runtime_error("Error: File " + _gamsFileName + " contains non cont,bin,int variables.");
                }
            }
            foundVarNumbers = true;
            continue;
        }

        // Number of fixed variables
        if (line.find("FX") != std::string::npos) {
            std::istringstream iss(line);
            std::string asterisk, FX;
            iss >> asterisk;
            iss >> FX;
            iss >> _nfixedVar;
            foundFixedVarNumber = true;
            continue;
        }

        // Find whether we minimize or maximize and get the variable
        if (line.find("Solve m") != std::string::npos) {
            std::size_t optSense = line.find("minimizing");
            if (optSense != std::string::npos) {
                _minimizing = true;
            }
            else {
                optSense    = line.find("maximizing");
                _minimizing = false;
            }
            _objName = line.substr(optSense + 11, line.length() - optSense - 12);
            _objNr   = std::stoi(_objName.substr(1, _objName.length()));
            foundObjective = true;
            continue;
        }
    }

    if (!foundEqNumbers) {
        throw std::runtime_error("Error: Did not find equation counts in file " + _gamsFileName + ".");
    }
    if (!foundVarNumbers) {
        throw std::runtime_error("Error: Did not find variable counts in file " + _gamsFileName + ".");
    }
    if (!foundFixedVarNumber) {
        throw std::runtime_error("Error: Did not find FX count in file " + _gamsFileName + ".");
    }
    if (!foundObjective) {
        throw std::runtime_error("Error: Did not find solve statement in file " + _gamsFileName + ".");
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting the variable names
void
MAiNGOReaderWriter::_extract_variable_names()
{
    // Get string with all variables
    size_t counter = 0;
    std::string variablesString = _file[counter];
    while ( (variablesString.find("Variables") == std::string::npos) || (variablesString[0] == '*') ) {
        counter++;
        variablesString = _file[counter];
    }
    while (variablesString.find(";") == std::string::npos) {
        counter++;
        std::string nextLine = _file[counter];
        while (nextLine[0] == ' ') {
            nextLine.erase(nextLine.begin());
        }
        variablesString = variablesString + nextLine;
    }
    // Get rid of the word Variables (at the beginning) and the semicolon (at the end)
    variablesString = variablesString.substr(9, variablesString.length() - 10);

    // Split variables string and save variable names + default initial values
    _initialPointCont.clear();
    _initialPointBin.clear();
    _initialPointInt.clear();
    _contVariables.clear();
    _binVariables.clear();
    _intVariables.clear();
    unsigned int pos = 0;
    while (pos < variablesString.length()) {
        if (variablesString[pos] == 'x') {
            pos++;
            std::string number;
            while (variablesString[pos] != ',' && pos < variablesString.length()) {
                number += variablesString[pos];
                pos++;
            }
            OptimizationVariable x;
            x.variableType = VT_CONTINUOUS;
            x.name         = "x" + number;
            x.lowerBound   = -1e10;
            x.upperBound   = 1e10;
            _contVariables.push_back(x);
            _initialPointCont.push_back(-1e10);
        }
        if (variablesString[pos] == 'b') {
            pos++;
            std::string number;
            while (variablesString[pos] != ',' && pos < variablesString.length()) {
                number += variablesString[pos];
                pos++;
            }
            OptimizationVariable b;
            b.variableType = VT_BINARY;
            b.name         = "b" + number;
            b.lowerBound   = 0;
            b.upperBound   = 1;
            _binVariables.push_back(b);
            _initialPointBin.push_back(0);
        }
        if (variablesString[pos] == 'i') {
            pos++;
            std::string number;
            while (variablesString[pos] != ',' && pos < variablesString.length()) {
                number += variablesString[pos];
                pos++;
            }
            OptimizationVariable i;
            i.variableType = VT_INTEGER;
            i.name         = "i" + number;
            i.lowerBound   = 0;
            i.upperBound   = 1e10;
            _intVariables.push_back(i);
            _initialPointInt.push_back(0);
        }
        pos++;
    }

    // Sanity checks: test if all variables have been found

	// Check if all continuous variables have been found
	if (_ncontVar != _contVariables.size()) {
		throw std::runtime_error("Error reading GAMS convert file: found " + std::to_string(_contVariables.size()) + " continuous variables but expected " + std::to_string(_ncontVar));
	}
    // Check if all integer variables have been found
    if (_nbinVar != _binVariables.size()) {
        pos     = 0;
        counter = 0;
        // Get string with binary variables
        std::string variablesString = _file[counter];
        while (variablesString.find("Binary Variables") == std::string::npos) {
            counter++;
            variablesString = _file[counter];
        }
        while (variablesString.find(";") == std::string::npos) {
            std::string nextLine;
            counter++;
            nextLine = _file[counter];
            while (nextLine[0] == ' ') {
                nextLine.erase(nextLine.begin());
            }
            variablesString = variablesString + nextLine;
        }
        // Get rid of the words Binary Variables and the colon ;
        variablesString = variablesString.substr(17, variablesString.length() - 18);
        while (pos < variablesString.length()) {
            if (variablesString[pos] == 'b') {
                pos++;
                std::string number;
                while (variablesString[pos] != ',' && variablesString[pos] != ';' && pos < variablesString.length()) {
                    number += variablesString[pos];
                    pos++;
                }
                std::string varName = 'b' + number;
                bool found          = false;
                for (std::size_t i = 0; i < _binVariables.size(); i++) {
                    if (_binVariables[i].name.compare(varName) == 0) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    OptimizationVariable b;
                    b.variableType = VT_BINARY;
                    b.name         = varName;
                    b.lowerBound   = 0;
                    b.upperBound   = 1;
                    _binVariables.push_back(b);
                    _initialPointBin.push_back(0);
                }
            }
            pos++;
        }
    }
	// Check if all binaries were found now:
	if (_nbinVar != _binVariables.size()) {
		throw std::runtime_error("Error reading GAMS convert file: found " + std::to_string(_binVariables.size()) + " binary variables but expected " + std::to_string(_nbinVar));
	}
    // Check if all integer variables have been found
    if (_nintVar != _intVariables.size()) {
        pos     = 0;
        counter = 0;
        // Get string with binary variables
        std::string variablesString = _file[counter];
        while (variablesString.find("Integer Variables") == std::string::npos) {
            counter++;
            variablesString = _file[counter];
        }
        while (variablesString.find(";") == std::string::npos) {
            std::string nextLine;
            counter++;
            nextLine = _file[counter];
            while (nextLine[0] == ' ') {
                nextLine.erase(nextLine.begin());
            }
            variablesString = variablesString + nextLine;
        }
        // Get rid of the words Binary Variables and the colon ;
        variablesString = variablesString.substr(18, variablesString.length() - 19);
        while (pos < variablesString.length()) {
            if (variablesString[pos] == 'i') {
                pos++;
                std::string number;
                while (variablesString[pos] != ',' && variablesString[pos] != ';' && pos < variablesString.length()) {
                    number += variablesString[pos];
                    pos++;
                }
                std::string varName = 'i' + number;
                bool found          = false;
                for (std::size_t i = 0; i < _intVariables.size(); i++) {
                    if (_intVariables[i].name.compare(varName) == 0) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    OptimizationVariable j;
                    j.variableType = VT_INTEGER;
                    j.name         = varName;
                    j.lowerBound   = 0;
                    j.upperBound   = 1e10;
                    _intVariables.push_back(j);
                    _initialPointInt.push_back(0);
                }
            }
            pos++;
        }
    }
	// Check if all integer variables were found now:
	if (_nintVar != _intVariables.size()) {
		throw std::runtime_error("Error reading GAMS convert file: found " + std::to_string(_intVariables.size()) + " integer variables but expected " + std::to_string(_nintVar));
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for getting total number of variables
void
MAiNGOReaderWriter::_extract_variable_bounds()
{

    std::string line;
    for (std::size_t ifile = 0; ifile < _file.size(); ifile++) {    // Read file line by line
        line = _file[ifile];
        // Check for positive variables
        if (line.find("Positive Variables") != std::string::npos || line.find("Positive variables") != std::string::npos || line.find("positive Variables") != std::string::npos || line.find("positive variables") != std::string::npos) {
            line = line.substr(std::string("Positive Variables").length(), line.length());
            // Get rid of white spaces
            while (line[0] == ' ') {
                line.erase(line.begin());
            }
            // Check if all variables are in one line
            while (line.find(";") == std::string::npos) {
                std::string nextLine;
                ifile++;
                nextLine = _file[ifile];
                while (nextLine[0] == ' ') {
                    nextLine.erase(nextLine.begin());
                }
                line = line + nextLine;
            }
            std::string variable;
            std::size_t posComma = static_cast<size_t>(line.find(","));
            while (posComma < line.length()) {
                variable = line.substr(0, posComma);
                line     = std::string(line.begin() + posComma + 1, line.end());
                posComma = line.find(",");
                _set_variable_lower_bound(variable, 0);
                _set_variable_initial_point(variable, 0);
            }
            variable = line.substr(0, line.length() - 1);
            _set_variable_lower_bound(variable, 0);
            _set_variable_initial_point(variable, 0);    // Set initial point default to 0
        }
        // Check for negative variables
        if (line.find("Negative Variables") != std::string::npos || line.find("Negative variables") != std::string::npos || line.find("negative Variables") != std::string::npos || line.find("negative variables") != std::string::npos) {
            line = line.substr(std::string("Negative Variables").length(), line.length());
            // Get rid of white spaces
            while (line[0] == ' ') {
                line.erase(line.begin());
            }
            // Check if all variables are in one line
            while (line.find(";") == std::string::npos) {
                std::string nextLine;
                ifile++;
                nextLine = _file[ifile];
                while (nextLine[0] == ' ') {
                    nextLine.erase(nextLine.begin());
                }
                line = line + nextLine;
            }
            std::string variable;
            std::size_t posComma = static_cast<size_t>(line.find(","));
            while (posComma < line.length()) {
                variable = line.substr(0, posComma);
                line     = std::string(line.begin() + posComma + 1, line.end());
                posComma = line.find(",");
                _set_variable_upper_bound(variable, 0);
                _set_variable_initial_point(variable, 0);
            }
            variable = line.substr(0, line.length() - 1);
            _set_variable_upper_bound(variable, 0);
            _set_variable_initial_point(variable, 0);    // Set initial point default to 0
        }
        // Check for non-default bounds
        // The check assumes that .lo comes always before .up in a line
        // Upper bounds
        if (line.find(".up") != std::string::npos || line.find(".UP") != std::string::npos || line.find(".uP") != std::string::npos || line.find(".Up") != std::string::npos) {
            std::string variable;
            double upperBound;
            unsigned int posColon = static_cast<unsigned int>(line.find(";"));
            // There is also a .lo in this line
            if (posColon < line.length() - 1) {
                std::string dummyLine = line.substr(posColon + 1, line.length());
                unsigned int posDot   = static_cast<unsigned int>(dummyLine.find("."));
                variable              = dummyLine.substr(0, posDot);
                while (variable[0] == ' ') {
                    variable.erase(variable.begin());
                }
                unsigned int posEq = static_cast<unsigned int>(dummyLine.find("="));
                std::cout.precision(16);
                upperBound = std::stod(dummyLine.substr(posEq + 1, dummyLine.length() - posEq - 1));

            }    // There is only .up in this line
            else {
                unsigned int posDot = static_cast<unsigned int>(line.find("."));
                variable            = line.substr(0, posDot);
                while (variable[0] == ' ') {
                    variable.erase(variable.begin());
                }
                unsigned int posEq = static_cast<unsigned int>(line.find("="));
                std::cout.precision(16);
                upperBound = std::stod(line.substr(posEq + 1, line.length() - posEq - 1));
            }
            _set_variable_upper_bound(variable, upperBound);
        }
        // Lower bounds
        if (line.find(".lo") != std::string::npos || line.find(".LO") != std::string::npos || line.find(".lO") != std::string::npos || line.find(".Lo") != std::string::npos) {
            std::string variable;
            double lowerBound;
            unsigned int posColon = static_cast<unsigned int>(line.find(";"));
            // There is also a .up in this line
            if (posColon < line.length() - 1) {
                std::string dummyLine = line.substr(0, posColon);
                unsigned int posDot   = static_cast<unsigned int>(dummyLine.find("."));
                variable              = dummyLine.substr(0, posDot);
                while (variable[0] == ' ') {
                    variable.erase(variable.begin());
                }
                unsigned int posEq = static_cast<unsigned int>(dummyLine.find("="));
                std::cout.precision(16);
                lowerBound = std::stod(dummyLine.substr(posEq + 1, dummyLine.length() - posEq - 1));

            }    // There is only .lo in this line
            else {
                unsigned int posDot = static_cast<unsigned int>(line.find("."));
                variable            = line.substr(0, posDot);
                while (variable[0] == ' ') {
                    variable.erase(variable.begin());
                }
                unsigned int posEq = static_cast<unsigned int>(line.find("="));
                std::cout.precision(16);
                lowerBound = std::stod(line.substr(posEq + 1, line.length() - posEq - 1));
            }
            _set_variable_lower_bound(variable, lowerBound);
            _set_variable_initial_point(variable, lowerBound);    // Set initial point default to lower bound
        }
        // Check for fixed variables
        if (line.find(".fx") != std::string::npos || line.find(".FX") != std::string::npos || line.find(".fX") != std::string::npos || line.find(".Fx") != std::string::npos) {
            double bound;
            unsigned int posDot  = static_cast<unsigned int>(line.find("."));
            std::string variable = line.substr(0, posDot);
            while (variable[0] == ' ') {
                variable.erase(variable.begin());
            }
            unsigned int posEq = static_cast<unsigned int>(line.find("="));
            std::cout.precision(16);
            bound = std::stod(line.substr(posEq + 1, line.length() - posEq - 1));
            _set_variable_lower_bound(variable, bound);
            _set_variable_upper_bound(variable, bound);
            _set_variable_initial_point(variable, bound);
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for reading the initial point from a GAMS convert file
void
MAiNGOReaderWriter::_extract_initial_point()
{

    std::string line;
    for (unsigned int ifile = 0; ifile < _file.size(); ifile++) {    // Read file line by line
        line = _file[ifile];
        // Check for initial point
        if (line.find(".l ") != std::string::npos || line.find(".l=") != std::string::npos) {
            double level;
            unsigned int posDot  = static_cast<unsigned int>(line.find("."));
            std::string variable = line.substr(0, posDot);
            while (variable[0] == ' ') {
                variable.erase(variable.begin());
            }
            unsigned int posEq = static_cast<unsigned int>(line.find("="));
            std::cout.precision(16);
            level              = std::stod(line.substr(posEq + 1, line.length() - posEq - 1));
            unsigned int varNr = _find_variable(variable);
            switch (variable[0]) {
                case 'x':
                    _initialPointCont[varNr] = level;
                    break;
                case 'b':
                    _initialPointBin[varNr] = static_cast<unsigned int>(level);
                    break;
                case 'i':
                    _initialPointInt[varNr] = static_cast<int>(level);
                    break;
                default:
                    std::cout << "WARNING: Unknown variable type. Spurious results are possible. Proceeding.";
                    break;
            }
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for reading the constraints from a GAMS convert file
void
MAiNGOReaderWriter::_extract_constraints()
{

    _constraints.resize(_ncons);
    std::string line;
    for (unsigned int ifile = 0; ifile < _file.size(); ifile++) {    // Read file line by line
        line = _file[ifile];
        if (line.find("..") != std::string::npos) {
            // Check if whole constraint is in one line
            while (line.find(";") == std::string::npos) {
                std::string nextLine;
                ifile++;
                nextLine = _file[ifile];
                while (nextLine[0] == ' ') {
                    nextLine.erase(nextLine.begin());
                }
                line = line + nextLine;
            }
            // Get rid of possible white spaces
            while (line[0] == ' ') {
                line.erase(line.begin());
            }

            unsigned int posDdots = static_cast<unsigned int>(line.find(".."));
            unsigned int conNr    = std::stoi(line.substr(1, posDdots - 1));
            std::string conName   = line.substr(0, posDdots);
            unsigned int posEq    = static_cast<unsigned int>(line.find("="));
            std::string lhs       = line.substr(posDdots + 2, posEq - posDdots - 2);
            while (lhs[0] == ' ') {
                lhs.erase(lhs.begin());
            }
            std::string rhs = line.substr(posEq + 3, line.length() - posEq - 4);
            while (rhs[0] == ' ') {
                rhs.erase(rhs.begin());
            }
            _constraints[conNr - 1].lhs  = lhs;
            _constraints[conNr - 1].rhs  = rhs;
            _constraints[conNr - 1].name = conName;
            // Check for equality constraint
            if (line.find("=E=") != std::string::npos || line.find("=e=") != std::string::npos) {
                _constraints[conNr - 1].constraintType = CT_EQ;
            }
            // Check for >= inequality constraint
            if (line.find("=G=") != std::string::npos || line.find("=g=") != std::string::npos) {
                _constraints[conNr - 1].constraintType = CT_GINEQ;
            }
            // Check for <= inequality constraint
            if (line.find("=L=") != std::string::npos || line.find("=l=") != std::string::npos) {
                _constraints[conNr - 1].constraintType = CT_LINEQ;
            }
        }
    }

    // Make log10 to log/log(10)
    _rename_logs(_constraints);

    // Make power and ** to pow
    _rename_powers(_constraints);

    // Get objective function
    unsigned int nObj = 0;
    unsigned int objCon;
	for (unsigned int i = 0; i < _ncons; i++) {
		if (_constraints[i].lhs.find(" " + _objName + " ") != std::string::npos || _constraints[i].lhs.find(" " + _objName) != std::string::npos || _constraints[i].lhs.find(_objName + " ") != std::string::npos) {
			if (_constraints[i].constraintType == CT_EQ) {
				nObj++;
			}
			else {
				nObj += 2;
			}
			// Check whether the objective is part of some operation, e.g., c*objective
			if (_constraints[i].lhs.find(" " + _objName + " ") != std::string::npos) {
				std::size_t pos = _constraints[i].lhs.find(" " + _objName + " ");
				if ((pos + _objName.length() + 2) < _constraints[i].lhs.length() && _constraints[i].lhs[pos + _objName.length() + 2] != '+' && _constraints[i].lhs[pos + _objName.length() + 2] != '-' || pos > 0 && _constraints[i].lhs[pos - 1] != '+' && _constraints[i].lhs[pos - 1] != '-') {
					nObj += 2;
				}
			}
			else if (_constraints[i].lhs.find(" " + _objName) != std::string::npos) {
				std::size_t pos = _constraints[i].lhs.find(" " + _objName);
				if ((pos + _objName.length() + 1) < _constraints[i].lhs.length() && _constraints[i].lhs[pos + _objName.length() + 1] != '+' && _constraints[i].lhs[pos + _objName.length() + 1] != '-' || _constraints[i].lhs[pos - 1] != '+' && _constraints[i].lhs[pos - 1] != '-') {
					nObj += 2;
				}
			}
			else {
				std::size_t pos = _constraints[i].lhs.find(_objName + " ");
				if (_constraints[i].lhs[pos + _objName.length() + 1] != '+' && _constraints[i].lhs[pos + _objName.length() + 1] != '-' || pos > 0 && _constraints[i].lhs[pos - 1] != '+' && _constraints[i].lhs[pos - 1] != '-') {
					nObj += 2;
				}		
			}
			objCon = i;
		}
		if (_constraints[i].rhs.find(" " + _objName + " ") != std::string::npos || _constraints[i].rhs.find(" " + _objName) != std::string::npos || _constraints[i].rhs.find(_objName + " ") != std::string::npos) {
			if (_constraints[i].constraintType == CT_EQ) {
				nObj++;
			}
			else {
				nObj += 2;
			}
			// Check whether the objective is part of some operation, e.g., c*objective
			if (_constraints[i].rhs.find(" " + _objName + " ") != std::string::npos) {
				std::size_t pos = _constraints[i].rhs.find(" " + _objName + " ");
				if ((pos + _objName.length() + 2) < _constraints[i].rhs.length() && _constraints[i].rhs[pos + _objName.length() + 2] != '+' && _constraints[i].rhs[pos + _objName.length() + 2] != '-' || pos > 0 && _constraints[i].rhs[pos - 1] != '+' && _constraints[i].rhs[pos - 1] != '-') { // check if before and after obj is + or -
					nObj += 2;
				}
			}
			else if (_constraints[i].rhs.find(" " + _objName) != std::string::npos) {
				std::size_t pos = _constraints[i].rhs.find(" " + _objName);
				if ((pos + _objName.length() + 1) < _constraints[i].rhs.length() && _constraints[i].rhs[pos + _objName.length() + 1] != '+' && _constraints[i].rhs[pos + _objName.length() + 1] != '-' || _constraints[i].rhs[pos - 1] != '+' && _constraints[i].rhs[pos - 1] != '-') { // check if before and after obj is + or -
					nObj += 2;
				}
			}
			else {
				std::size_t pos = _constraints[i].rhs.find(_objName + " ");
				if (_constraints[i].rhs[pos + _objName.length() + 1] != '+' && _constraints[i].rhs[pos + _objName.length() + 1] != '-' || pos > 0 && _constraints[i].rhs[pos - 1] != '+' && _constraints[i].rhs[pos - 1] != '-') { // check if before and after obj is + or -
					nObj += 2;
				}
			}
			objCon = i;
		}
	}
    _objSingle = (nObj > 1) ? false : true;
    if (_objSingle) {
        unsigned int posObj;
        unsigned int additionalLength = 0;
        if (_constraints[objCon].lhs.find(" " + _objName + " ") != std::string::npos || _constraints[objCon].lhs.find(" " + _objName) != std::string::npos || _constraints[objCon].lhs.find(_objName + " ") != std::string::npos) {
			if (_constraints[objCon].lhs.find(" " + _objName + " ") != std::string::npos) {
                posObj           = static_cast<unsigned int>(_constraints[objCon].lhs.find(" " + _objName + " "));
                additionalLength = 2;
            }
            else if (_constraints[objCon].lhs.find(" " + _objName) != std::string::npos) {
                posObj           = static_cast<unsigned int>(_constraints[objCon].lhs.find(" " + _objName));
                additionalLength = 1;
            }
            else {
                posObj           = static_cast<unsigned int>(_constraints[objCon].lhs.find(_objName + " "));			
                additionalLength = 1;
            }
            posObj = posObj == 0 ? posObj + 1 : posObj;
            if (_constraints[objCon].lhs[posObj - 1] == '+') {
                _constraints[objCon].lhs.erase(posObj - 1, 1 + additionalLength + _objName.length());
                if (_constraints[objCon].rhs.compare("0") == 0) {
                    _objFunction = "-(" + _constraints[objCon].lhs + ")";
                }
                else {
                    _objFunction = _constraints[objCon].rhs + " - (" + _constraints[objCon].lhs + ")";
                }
            }
            else if (_constraints[objCon].lhs[posObj - 1] == '-') {
                _constraints[objCon].lhs.erase(posObj - 1, 1 + additionalLength + _objName.length());
                if (_constraints[objCon].rhs.compare("0") == 0) {
                    _objFunction = _constraints[objCon].lhs;
                }
                else {
                    _objFunction = _constraints[objCon].lhs + " - (" + _constraints[objCon].rhs + ")";
                }
            }
            else {
                _constraints[objCon].lhs.erase(posObj - 1, 1 + additionalLength + _objName.length());
                if (_constraints[objCon].rhs.compare("0") == 0) {
                    _objFunction = "-(" + _constraints[objCon].lhs + ")";
                }
                else {
                    _objFunction = _constraints[objCon].rhs + " - (" + _constraints[objCon].lhs + ")";
                }
            }
        }
        else {
            if (_constraints[objCon].rhs.find(" " + _objName + " ") != std::string::npos) {
                posObj           = static_cast<unsigned int>(_constraints[objCon].rhs.find(" " + _objName + " "));
                additionalLength = 2;
            }
            else if (_constraints[objCon].rhs.find(" " + _objName) != std::string::npos) {
                posObj           = static_cast<unsigned int>(_constraints[objCon].rhs.find(" " + _objName));
                additionalLength = 1;
            }
            else {
                posObj           = static_cast<unsigned int>(_constraints[objCon].rhs.find(_objName + " "));
                additionalLength = 1;
            }
            posObj = posObj == 0 ? posObj + 1 : posObj;
            if (_constraints[objCon].rhs[posObj - 1] == '+') {
                _constraints[objCon].rhs.erase(posObj - 1, 1 + additionalLength + _objName.length());
                if (_constraints[objCon].lhs.compare("0") == 0) {
                    _objFunction = "-(" + _constraints[objCon].rhs + ")";
                }
                else {
                    _objFunction = "-(" + _constraints[objCon].rhs + ") + (" + _constraints[objCon].lhs + ")";
                }
            }
            else if (_constraints[objCon].rhs[posObj - 1] == '-') {
                _constraints[objCon].rhs.erase(posObj - 1, 1 + additionalLength + _objName.length());
                if (_constraints[objCon].lhs.compare("0") == 0) {
                    _objFunction = _constraints[objCon].rhs;
                }
                else {
                    _objFunction = _constraints[objCon].rhs + " - (" + _constraints[objCon].lhs + ")";
                }
            }
			else {
				_constraints[objCon].rhs.erase(posObj - 1, 1 + additionalLength + _objName.length());
				if (_constraints[objCon].lhs.compare("0") == 0) {
					_objFunction = "-(" + _constraints[objCon].rhs + ")";
				}
				else {
					_objFunction = "-(" + _constraints[objCon].rhs + ") + (" + _constraints[objCon].lhs + ")";
				}
			}
        }
        _constraints.erase(_constraints.begin() + objCon);
        _ncons--;
        _neq--;
        // Remove the objective variable
        for (unsigned int i = 0; i < _ncontVar; i++) {
            if (_contVariables[i].name.compare(_objName) == 0) {
                _contVariables.erase(_contVariables.begin() + i);
                _ncontVar--;
                _initialPointCont.erase(_initialPointCont.begin() + i);
                break;
            }
        }
        for (unsigned int i = 0; i < _nbinVar; i++) {
            if (_binVariables[i].name.compare(_objName) == 0) {
                _binVariables.erase(_binVariables.begin() + i);
                _nbinVar--;
                _initialPointBin.erase(_initialPointBin.begin() + i);
                break;
            }
        }
        for (unsigned int i = 0; i < _nintVar; i++) {
            if (_intVariables[i].name.compare(_objName) == 0) {
                _intVariables.erase(_intVariables.begin() + i);
                _nintVar--;
                _initialPointInt.erase(_initialPointInt.begin() + i);
                break;
            }
        }
        _nvar--;
    }
    else {
        _objFunction = _objName;
    }

    if (!_minimizing) {
        _objFunction = "-(" + _objFunction + ")";
    }

    // Sort the constraints
    for (unsigned int i = 0; i < _ncons; i++) {
        switch (_constraints[i].constraintType) {
            case CT_EQ:
                _eqConstraints.push_back(_constraints[i]);
                break;
            case CT_GINEQ:
                _gineqConstraints.push_back(_constraints[i]);
                break;
            case CT_LINEQ:
                _lineqConstraints.push_back(_constraints[i]);
                break;
            default:
                std::cout << "WARNING: Unknown constraint type. Spurious results are possible. Proceeding.";
                break;
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for obtaining the correct vector index
unsigned int
MAiNGOReaderWriter::_find_variable(const std::string &varName)
{

    unsigned int index = 0;
    switch (varName[0]) {
        case 'x':
            for (unsigned int i = 0; i < _ncontVar; i++) {
                if (_contVariables[i].name.compare(varName) == 0) {
                    index = i;
                    break;
                }
            }
            break;
        case 'b':
            for (unsigned int i = 0; i < _nbinVar; i++) {
                if (_binVariables[i].name.compare(varName) == 0) {
                    index = i;
                    break;
                }
            }
            break;
        case 'i':
            for (unsigned int i = 0; i < _nintVar; i++) {
                if (_intVariables[i].name.compare(varName) == 0) {
                    index = i;
                    break;
                }
            }
            break;
        default:
            std::cout << "WARNING: Unknown variable type. Spurious results are possible. Proceeding.";
            break;
    }
    return index;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for setting the lower bound of a variable
void
MAiNGOReaderWriter::_set_variable_lower_bound(const std::string &varName, const double lowerBound)
{

    unsigned int varNr = _find_variable(varName);
    switch (varName[0]) {
        case 'x':
            _contVariables[varNr].lowerBound = lowerBound;
            break;
        case 'b':
            _binVariables[varNr].lowerBound = lowerBound;
            break;
        case 'i':
            _intVariables[varNr].lowerBound = lowerBound;
            break;
        default:
            std::cout << "WARNING: Unknown variable type. Spurious results are possible. Proceeding.";
            break;
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for setting the upper bound of a variable
void
MAiNGOReaderWriter::_set_variable_upper_bound(const std::string &varName, const double upperBound)
{

    unsigned int varNr = _find_variable(varName);
    switch (varName[0]) {
        case 'x':
            _contVariables[varNr].upperBound = upperBound;
            break;
        case 'b':
            _binVariables[varNr].upperBound = upperBound;
            break;
        case 'i':
            _intVariables[varNr].upperBound = upperBound;
            break;
        default:
            std::cout << "WARNING: Unknown variable type. Spurious results are possible. Proceeding.";
            break;
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for setting the initial point of a variable
void
MAiNGOReaderWriter::_set_variable_initial_point(const std::string &varName, const double value)
{

    unsigned int varNr = _find_variable(varName);
    switch (varName[0]) {
        case 'x':
            _initialPointCont[varNr] = value;
            break;
        case 'b':
            _initialPointBin[varNr] = (unsigned int)value;
            break;
        case 'i':
            _initialPointInt[varNr] = (int)value;
            break;
        default:
            std::cout << "WARNING: Unknown variable type. Spurious results are possible. Proceeding.";
            break;
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for renaming power and ** to pow for constraints
void
MAiNGOReaderWriter::_rename_powers(std::vector<Constraint> &constraints)
{

    for (unsigned int i = 0; i < constraints.size(); i++) {
        std::size_t posPow = 0;
        // LHS first
        // power to pow
        posPow = constraints[i].lhs.find("power");
        while (posPow != std::string::npos) {
            constraints[i].lhs.erase(posPow + 3, 2);
            posPow = constraints[i].lhs.find("power");
        }
        // POWER to pow
        posPow = constraints[i].lhs.find("POWER");
        while (posPow != std::string::npos) {
            constraints[i].lhs[posPow]     = 'p';
            constraints[i].lhs[posPow + 1] = 'o';
            constraints[i].lhs[posPow + 2] = 'w';
            constraints[i].lhs.erase(posPow + 3, 2);
            posPow = constraints[i].lhs.find("POWER");
        }
        // ** to pow
        _rename_double_asterisk(constraints[i].lhs);

        // RHS second
        // power to pow
        posPow = constraints[i].rhs.find("power");
        while (posPow != std::string::npos) {
            constraints[i].rhs.erase(posPow + 3, 2);
            posPow = constraints[i].rhs.find("power");
        }
        // POWER to pow
        posPow = constraints[i].rhs.find("POWER");
        while (posPow != std::string::npos) {
            constraints[i].rhs[posPow]     = 'p';
            constraints[i].rhs[posPow + 1] = 'o';
            constraints[i].rhs[posPow + 2] = 'w';
            constraints[i].rhs.erase(posPow + 3, 2);
            posPow = constraints[i].rhs.find("POWER");
        }
        // ** to pow
        _rename_double_asterisk(constraints[i].rhs);
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for renaming ** to pow in a string
void
MAiNGOReaderWriter::_rename_double_asterisk(std::string &str)
{

    std::size_t posPow = str.find("**");
    while (posPow != std::string::npos) {
        // Exponent
        str.erase(posPow, 1);
        str[posPow]               = ',';
        unsigned int nParentheses = 0;
        bool inParentheses        = false;
        unsigned int pos          = static_cast<unsigned int>(posPow + 1);
        if (str[pos] == '(') {
            inParentheses = true;
            nParentheses++;
        }
        // We can recognize exp, log, pow, sin, max, min, tan, cos, abs
        if (str[pos] == 'e' || str[pos] == 'l' || str[pos] == 'p' || str[pos] == 's' || str[pos] == 'm' || str[pos] == 't' || str[pos] == 'c' || str[pos] == 'a') {
            inParentheses = true;
            nParentheses++;
            pos += 3;
        }
        if (inParentheses) {
            // Find correct closing parentheses
            while (nParentheses > 0) {
                pos++;
                if (str[pos] == '(') {
                    nParentheses++;
                }
                if (str[pos] == ')') {
                    nParentheses--;
                }
            }
            str.insert(pos + 1, ")");
        }
        else {
            // Find a whitespace or any operation
            while (str[pos] != ' ' && str[pos] != '*' && str[pos] != '/' && str[pos] != '+' && str[pos] != '-' && pos < str.length()) {
                pos++;
            }
            str.insert(pos, ")");
        }

        //Base
        inParentheses = false;
        nParentheses  = 0;
        pos           = static_cast<unsigned int>(posPow - 1);
        if (str[pos] == ')') {
            inParentheses = true;
            nParentheses++;
        }
        // if(str[pos]=='e' || str[pos]=='l' || str[pos]=='p' || str[pos]=='s'
        // || str[pos]=='m' || str[pos]=='t' || str[pos]=='c' || str[pos]=='a'){
        // inParentheses = true;
        // nParentheses++;
        // pos+=3;
        // }
        if (inParentheses) {
            // Find correct closing parentheses
            while (nParentheses > 0) {
                pos--;
                if (str[pos] == '(') {
                    nParentheses--;
                }
                if (str[pos] == ')') {
                    nParentheses++;
                }
            }
            // We can recognize exp, log, pow, sin, max, min, tan, cos, abs
			if (pos != 0) {
				if (str[pos - 1] == 'p' || str[pos - 1] == 'g' || str[pos - 1] == 'w' || str[pos - 1] == 'n' || str[pos - 1] == 'x' || str[pos - 1] == 's') {
					pos -= 3;
				}
			}
            str.insert(pos, "pow(");
        }
        else {
            // Find a whitespace or any operation or possibly an opening parenthesis
            while (str[pos] != ' ' && str[pos] != '*' && str[pos] != '/' && str[pos] != '+' && str[pos] != '-' && str[pos] != '(' && pos > 0) {
                pos--;
            }
            if (pos != 0 || (pos==0 && str[pos] == '-')) {
                str.insert(pos + 1, "pow(");
            }
            else {
                str.insert(pos, "pow(");
            }
        }
        posPow = str.find("**");
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for renaming log10 to log/log(10.) for constraints
void
MAiNGOReaderWriter::_rename_logs(std::vector<Constraint> &constraints)
{

    for (unsigned int i = 0; i < constraints.size(); i++) {
        std::size_t posLog = 0;
        // LHS first
        // log10 to log
        posLog = constraints[i].lhs.find("log10");
        while (posLog != std::string::npos) {
            constraints[i].lhs[posLog]     = '(';
            constraints[i].lhs[posLog + 1] = 'l';
            constraints[i].lhs[posLog + 2] = 'o';
            constraints[i].lhs[posLog + 3] = 'g';
            constraints[i].lhs.erase(posLog + 4, 1);
            _add_division_log10(constraints[i].lhs, posLog);
            posLog = constraints[i].lhs.find("log10");
        }
        // LOG10 to log
        posLog = constraints[i].lhs.find("LOG10");
        while (posLog != std::string::npos) {
            constraints[i].lhs[posLog]     = '(';
            constraints[i].lhs[posLog + 1] = 'l';
            constraints[i].lhs[posLog + 2] = 'o';
            constraints[i].lhs[posLog + 3] = 'g';
            constraints[i].lhs.erase(posLog + 4, 1);
            _add_division_log10(constraints[i].lhs, posLog);
            posLog = constraints[i].lhs.find("LOG10");
        }

        // RHS second
        // log10 to log
        posLog = constraints[i].rhs.find("log10");
        while (posLog != std::string::npos) {
            constraints[i].lhs[posLog]     = '(';
            constraints[i].lhs[posLog + 1] = 'l';
            constraints[i].lhs[posLog + 2] = 'o';
            constraints[i].lhs[posLog + 3] = 'g';
            constraints[i].lhs.erase(posLog + 4, 1);
            _add_division_log10(constraints[i].lhs, posLog);
            posLog = constraints[i].rhs.find("log10");
        }
        // LOG10 to log
        posLog = constraints[i].rhs.find("LOG10");
        while (posLog != std::string::npos) {
            constraints[i].lhs[posLog]     = '(';
            constraints[i].lhs[posLog + 1] = 'l';
            constraints[i].lhs[posLog + 2] = 'o';
            constraints[i].lhs[posLog + 3] = 'g';
            constraints[i].lhs.erase(posLog + 4, 1);
            _add_division_log10(constraints[i].lhs, posLog);
            posLog = constraints[i].rhs.find("LOG10");
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for adding the /log(10.) after finding a log10 operator
void
MAiNGOReaderWriter::_add_division_log10(std::string &str, const std::size_t posLog)
{
    // Find closing parantheses
    std::size_t pos           = posLog + 5;
    unsigned int nParentheses = 1;
    while (nParentheses > 0) {
        pos++;
        if (str[pos] == '(') {
            nParentheses++;
        }
        if (str[pos] == ')') {
            nParentheses--;
        }
    }
    str.insert(pos + 1, "/log(10.))");
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for removing characters saved in charsToRemove from str
void
MAiNGOReaderWriter::_remove_chars_from_string(std::string &str, const char *charsToRemove)
{
    for (std::size_t i = 0; i < std::strlen(charsToRemove); i++) {
        str.erase(std::remove(str.begin(), str.end(), charsToRemove[i]), str.end());
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
// function for renaming a given variables in a given constraint
void
MAiNGOReaderWriter::_replace_all_variables_in_constraint(std::string &constraint, const std::string &varName, const std::string &newName)
{
    std::size_t posVariable = constraint.find(varName);
    while (posVariable != std::string::npos) {
        bool isItReallyTheVariable = !isdigit(constraint[posVariable + varName.length()]);
        if (isItReallyTheVariable && posVariable > 0) {
            char c = constraint[posVariable - 1];
            if (isalpha(c) || c == '_') {
                isItReallyTheVariable = false;
            }
        }
        if (isItReallyTheVariable) {
            constraint.replace(posVariable, varName.length(), newName);
        }
        posVariable = constraint.find(varName, posVariable + varName.length());
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for renaming variables and constraints w.r.t. a pre-read dictionary file
void
MAiNGOReaderWriter::_rename_variables_and_constraints()
{
    // No dictionary file has been provided
    if (_dict.empty()) {
        return;
    }

    // Rename continuous variables
    for (unsigned int i = 0; i < _ncontVar; i++) {
        for (std::size_t j = 0; j < _dict.size(); j++) {
            std::size_t pos = _dict[j].find("  " + _contVariables[i].name + "  ");
            if (pos != std::string::npos) {
                std::string newName = _dict[j].substr(pos + _contVariables[i].name.length() + 4 /*4 whitespaces*/, std::string::npos);
                if (newName.compare(_contVariables[i].name) != 0) {    // continue only if they are not equal
                    _remove_chars_from_string(newName, "(),+-");
                    // Change the variable names in all constraints -- Don't change the vector holding all constraints
                    for (unsigned int k = 0; k < _nlineq; k++) {
                        _replace_all_variables_in_constraint(_lineqConstraints[k].lhs, _contVariables[i].name, newName);
                        if (_lineqConstraints[k].rhs.compare("0") != 0) {
                            _replace_all_variables_in_constraint(_lineqConstraints[k].rhs, _contVariables[i].name, newName);
                        }
                    }
                    for (unsigned int k = 0; k < _ngineq; k++) {
                        _replace_all_variables_in_constraint(_gineqConstraints[k].lhs, _contVariables[i].name, newName);
                        if (_gineqConstraints[k].rhs.compare("0") != 0) {
                            _replace_all_variables_in_constraint(_gineqConstraints[k].rhs, _contVariables[i].name, newName);
                        }
                    }
                    for (unsigned int k = 0; k < _neq; k++) {
                        _replace_all_variables_in_constraint(_eqConstraints[k].lhs, _contVariables[i].name, newName);
                        if (_eqConstraints[k].rhs.compare("0") != 0) {
                            _replace_all_variables_in_constraint(_eqConstraints[k].rhs, _contVariables[i].name, newName);
                        }
                    }
                    _replace_all_variables_in_constraint(_objFunction, _contVariables[i].name, newName);
                    _contVariables[i].name = newName;
                }
            }
        }
    }
    // Rename binary variables
    for (unsigned int i = 0; i < _nbinVar; i++) {
        for (std::size_t j = 0; j < _dict.size(); j++) {
            std::size_t pos = _dict[j].find("  " + _binVariables[i].name + "  ");
            if (pos != std::string::npos) {
                std::string newName = _dict[j].substr(pos + _binVariables[i].name.length() + 4 /*4 whitespaces*/, std::string::npos);
                if (newName.compare(_binVariables[i].name) != 0) {    // continue only if they are not equal
                    _remove_chars_from_string(newName, "(),+-");
                    // Change the variable names in all constraints -- Don't change the vector holding all constraints
                    for (unsigned int k = 0; k < _nlineq; k++) {
                        _replace_all_variables_in_constraint(_lineqConstraints[k].lhs, _binVariables[i].name, newName);
                        if (_lineqConstraints[k].rhs.compare("0") != 0) {
                            _replace_all_variables_in_constraint(_lineqConstraints[k].rhs, _binVariables[i].name, newName);
                        }
                    }
                    for (unsigned int k = 0; k < _ngineq; k++) {
                        _replace_all_variables_in_constraint(_gineqConstraints[k].lhs, _binVariables[i].name, newName);
                        if (_gineqConstraints[k].rhs.compare("0") != 0) {
                            _replace_all_variables_in_constraint(_gineqConstraints[k].rhs, _binVariables[i].name, newName);
                        }
                    }
                    for (unsigned int k = 0; k < _neq; k++) {
                        _replace_all_variables_in_constraint(_eqConstraints[k].lhs, _binVariables[i].name, newName);
                        if (_eqConstraints[k].rhs.compare("0") != 0) {
                            _replace_all_variables_in_constraint(_eqConstraints[k].rhs, _binVariables[i].name, newName);
                        }
                    }
                    _replace_all_variables_in_constraint(_objFunction, _binVariables[i].name, newName);
                    _binVariables[i].name = newName;
                }
            }
        }
    }
    // Rename integer variables
    for (unsigned int i = 0; i < _nintVar; i++) {
        for (std::size_t j = 0; j < _dict.size(); j++) {
            std::size_t pos = _dict[j].find("  " + _intVariables[i].name + "  ");
            if (pos != std::string::npos) {
                std::string newName = _dict[j].substr(pos + _intVariables[i].name.length() + 4 /*4 whitespaces*/, std::string::npos);
                if (newName.compare(_intVariables[i].name) != 0) {    // continue only if they are not equal
                    _remove_chars_from_string(newName, "(),+-");
                    // Change the variable names in all constraints -- Don't change the vector holding all constraints
                    for (unsigned int k = 0; k < _nlineq; k++) {
                        _replace_all_variables_in_constraint(_lineqConstraints[k].lhs, _intVariables[i].name, newName);
                        if (_lineqConstraints[k].rhs.compare("0") != 0) {
                            _replace_all_variables_in_constraint(_lineqConstraints[k].rhs, _intVariables[i].name, newName);
                        }
                    }
                    for (unsigned int k = 0; k < _ngineq; k++) {
                        _replace_all_variables_in_constraint(_gineqConstraints[k].lhs, _intVariables[i].name, newName);
                        if (_gineqConstraints[k].rhs.compare("0") != 0) {
                            _replace_all_variables_in_constraint(_gineqConstraints[k].rhs, _intVariables[i].name, newName);
                        }
                    }
                    for (unsigned int k = 0; k < _neq; k++) {
                        _replace_all_variables_in_constraint(_eqConstraints[k].lhs, _intVariables[i].name, newName);
                        if (_eqConstraints[k].rhs.compare("0") != 0) {
                            _replace_all_variables_in_constraint(_eqConstraints[k].rhs, _intVariables[i].name, newName);
                        }
                    }
                    _replace_all_variables_in_constraint(_objFunction, _intVariables[i].name, newName);
                    _intVariables[i].name = newName;
                }
            }
        }
    }
    // Rename <= inequalities
    for (unsigned int i = 0; i < _nlineq; i++) {
        for (std::size_t j = 0; j < _dict.size(); j++) {
            std::size_t pos = _dict[j].find("  " + _lineqConstraints[i].name + "  ");
            if (pos != std::string::npos) {
                std::string newName = _dict[j].substr(pos + _lineqConstraints[i].name.length() + 4 /*4 whitespaces*/, std::string::npos);
                if (newName.compare(_lineqConstraints[i].name) != 0) {    // continue only if they are not equal
                    _remove_chars_from_string(newName, "()");
                    _lineqConstraints[i].name = newName;
                }
            }
        }
    }
    // Rename >= inequalities
    for (unsigned int i = 0; i < _ngineq; i++) {
        for (std::size_t j = 0; j < _dict.size(); j++) {
            std::size_t pos = _dict[j].find("  " + _gineqConstraints[i].name + "  ");
            if (pos != std::string::npos) {
                std::string newName = _dict[j].substr(pos + _gineqConstraints[i].name.length() + 4 /*4 whitespaces*/, std::string::npos);
                if (newName.compare(_gineqConstraints[i].name) != 0) {    // continue only if they are not equal
                    _remove_chars_from_string(newName, "()");
                    _gineqConstraints[i].name = newName;
                }
            }
        }
    }

    // Rename equalities
    for (unsigned int i = 0; i < _neq; i++) {
        for (std::size_t j = 0; j < _dict.size(); j++) {
            std::size_t pos = _dict[j].find("  " + _eqConstraints[i].name + "  ");
            if (pos != std::string::npos) {
                std::string newName = _dict[j].substr(pos + _eqConstraints[i].name.length() + 4 /*4 whitespaces*/, std::string::npos);
                if (newName.compare(_eqConstraints[i].name) != 0) {    // continue only if they are not equal
                    _remove_chars_from_string(newName, "()");
                    _eqConstraints[i].name = newName;
                }
            }
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for writing MAiNGO problem.h file
void
MAiNGOReaderWriter::write_MAiNGO_problem_file(const std::string &problemFileName, const std::string &dictFileName)
{

    _dict.clear();
    std::ifstream dictFile;
    dictFile.open(dictFileName);
    if (dictFile.is_open()) {
        std::string line;
        while (std::getline(dictFile, line)) {    // Read file line by line
            const auto strEnd = line.find_last_not_of(" ");
            line              = line.substr(0, strEnd + 1);
            _dict.push_back(line);
        }
        _dict.erase(_dict.begin(), _dict.begin() + 15);
        std::cout << "Using dict file " << dictFileName << " to rename variables and equations in the MAiNGO problem file." << std::endl;
    }
    else {    // File not found
        std::cout << "Warning: Could not open GAMS dictionary file " << dictFileName << " while writing MAiNGO problem file.\nProceeding with default variable and constraint names." << std::endl;
    }
    dictFile.close();

    // Make temporary copies of the variables and constraints, since we will rename them
    std::vector<OptimizationVariable> tmpContVariables = _contVariables;
    std::vector<OptimizationVariable> tmpBinVariables  = _binVariables;
    std::vector<OptimizationVariable> tmpIntVariables  = _intVariables;
    std::vector<Constraint> tmpEqConstraints           = _eqConstraints;
    std::vector<Constraint> tmpGineqConstraints        = _gineqConstraints;
    std::vector<Constraint> tmpLineqConstraints        = _lineqConstraints;

    _rename_variables_and_constraints();

    std::string outFileName = problemFileName + ".h";
    std::ofstream outFile(outFileName);

    _write_MAiNGO_model(outFile, outFileName);

    _write_MAiNGO_variables(outFile);

    _write_MAiNGO_initial_point(outFile);

    _write_MAiNGO_constructor(outFile);

    _write_MAiNGO_evaluate(outFile);

    outFile.close();

    // Restore original names
    _contVariables    = tmpContVariables;
    _binVariables     = tmpBinVariables;
    _intVariables     = tmpIntVariables;
    _eqConstraints    = tmpEqConstraints;
    _gineqConstraints = tmpGineqConstraints;
    _lineqConstraints = tmpLineqConstraints;

    std::cout << "MAiNGO problem file written successfully." << std::endl;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for reading in all GAMS files in folder input and writing MAiNGO problem files to folder output
void
MAiNGOReaderWriter::convert_GAMS_folder_to_MAiNGO_problem(const std::string &input, const std::string &output)
{

    std::vector<std::string> gamsFileNames;
    std::vector<std::string> pathsToGamsFiles;
    std::vector<std::string> MAiNGOproblemFilesNames;
#ifdef GCC_FS_EXPERIMENTAL
    for (auto &p : std::experimental::filesystem::directory_iterator(input)) {
        std::experimental::filesystem::path pa = p;
#else
    for (auto &p : std::filesystem::directory_iterator(input)) {
        std::filesystem::path pa = p;
#endif
        pathsToGamsFiles.push_back(pa.string());
        gamsFileNames.push_back(pa.string().substr(input.length() + 1, pa.string().length()));
        MAiNGOproblemFilesNames.push_back("problem_" + gamsFileNames.back().substr(0, gamsFileNames.back().length() - 4));
    }

    if (gamsFileNames.size() < 1) {
        std::cout << "No GAMS files found in " << input << "." << std::endl;
    }

    for (std::size_t i = 0; i < gamsFileNames.size(); i++) {
        read_GAMS_convert_file(pathsToGamsFiles[i]);
        write_MAiNGO_problem_file(output + "\\" + MAiNGOproblemFilesNames[i]);
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for reading in all GAMS files in folder input and writing MAiNGO problem files to folder output
void
MAiNGOReaderWriter::convert_GAMS_folder_to_ALE_problem(const std::string &input, const std::string &output)
{

    std::vector<std::string> gamsFileNames;
    std::vector<std::string> pathsToGamsFiles;
    std::vector<std::string> ALEproblemFilesNames;
#ifdef GCC_FS_EXPERIMENTAL
    for (auto &p : std::experimental::filesystem::directory_iterator(input)) {
        std::experimental::filesystem::path pa = p;
#else
    for (auto &p : std::filesystem::directory_iterator(input)) {
        std::filesystem::path pa = p;
#endif
        pathsToGamsFiles.push_back(pa.string());
        gamsFileNames.push_back(pa.string().substr(input.length() + 1, pa.string().length()));
        ALEproblemFilesNames.push_back("problem_" + gamsFileNames.back().substr(0, gamsFileNames.back().length() - 4));
    }

    if (gamsFileNames.size() < 1) {
        std::cout << "No GAMS files found in " << input << "." << std::endl;
    }

    for (std::size_t i = 0; i < gamsFileNames.size(); i++) {
        read_GAMS_convert_file(pathsToGamsFiles[i]);
        write_ALE_problem_file(output + "\\" + ALEproblemFilesNames[i]);
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for printing all problem info
void
MAiNGOReaderWriter::print_problem_info_GAMS_file(std::ostream &outstream)
{

    outstream << "----- Problem Information from GAMS convert file " << _gamsFileName << " -----" << std::endl
              << std::endl;

    outstream << std::setw(37) << "Total number of variables: " << _nvar << std::endl;
    outstream << std::setw(37) << "Number of continuous variables: " << _ncontVar << std::endl;
    outstream << std::setw(37) << "Number of binary variables: " << _nbinVar << std::endl;
    outstream << std::setw(37) << "Number of integer variables: " << _nintVar << std::endl;

    outstream << std::endl;

    outstream << std::setw(37) << "Total number of constraints: " << _ncons << std::endl;
    outstream << std::setw(37) << "Number of equality constraints: " << _neq << std::endl;
    outstream << std::setw(37) << "Number of >= inequality constraints: " << _ngineq << std::endl;
    outstream << std::setw(37) << "Number of <= inequality constraints: " << _nlineq << std::endl;

    outstream << std::endl;

    outstream << std::setw(37) << "Number of fixed variables: " << _nfixedVar << std::endl;

    outstream << std::endl;

    if (_minimizing) {
        outstream << std::setw(37) << "Minimizing function: " << _objFunction << std::endl;
    }
    else {
        outstream << std::setw(37) << "Maximizing function: " << _objFunction << std::endl;
    }
    outstream << "----------------------------------------------------------------" << std::endl;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for writing Model to problem.h
void
MAiNGOReaderWriter::_write_MAiNGO_model(std::ofstream &outputFile, const std::string &problemName)
{

    std::time_t t = std::time(0);    // get time now
    std::tm *now  = std::localtime(&t);

    outputFile << "/**********************************************************************************\n";
    outputFile << " * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University\n";
    outputFile << " *\n";
    outputFile << " * MAiNGO and the accompanying materials are made available under the\n";
    outputFile << " * terms of the Eclipse Public License 2.0 which is available at\n";
    outputFile << " * http://www.eclipse.org/legal/epl-2.0.\n";
    outputFile << " *\n";
    outputFile << " * SPDX-License-Identifier: EPL-2.0\n";
    outputFile << " *\n";
    outputFile << " * @file " << problemName << ".h\n";
    outputFile << " *\n";
    outputFile << " * @brief This file has been automatically generated by the MAiNGO Reader Writer\n";
    outputFile << " *        utility.\n";
    outputFile << " *\n";
    outputFile << " **********************************************************************************/\n";
    outputFile << "\n";


    outputFile << "#pragma once\n";
    outputFile << "\n";
    outputFile << "#include \"MAiNGOmodel.h\"\n";
    outputFile << "\n";
    outputFile << "\n";
    outputFile << "using Var = mc::FFVar;    // This allows us to write Var instead of mc::FFVar\n";
    outputFile << "\n";
    outputFile << "\n";
    outputFile << "/**\n";
    outputFile << "* @class Model\n";
    outputFile << "* @brief Class defining the actual model to be solved by MAiNGO \n";
    outputFile << "*/\n";
    outputFile << "class Model: public maingo::MAiNGOmodel {\n";
    outputFile << "\n";
    outputFile << "    public:\n";
    outputFile << "        /**\n";
    outputFile << "        * @brief Default constructor \n";
    outputFile << "        */\n";
    outputFile << "        Model();\n";
    outputFile << "\n";
    outputFile << "        /**\n";
    outputFile << "        * @brief Main function used to evaluate the model and construct a directed acyclic graph\n";
    outputFile << "        *\n";
    outputFile << "        * @param[in] optVars is the optimization variables vector\n";
    outputFile << "        */  \n";
    outputFile << "        maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);\n";
    outputFile << "\n";
    outputFile << "        /**\n";
    outputFile << "        * @brief Function for getting optimization variables data\n";
    outputFile << "        */\n";
    outputFile << "        std::vector<maingo::OptimizationVariable> get_variables();\n";
    outputFile << "\n";
    outputFile << "        /**\n";
    outputFile << "        * @brief Function for getting initial point data\n";
    outputFile << "        */\n";
    outputFile << "        std::vector<double> get_initial_point();\n";
    outputFile << "\n";
    outputFile << "    private:\n";
    outputFile << "};\n";
    outputFile << "\n";
    outputFile << "\n";
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for writing variables to problem.h
void
MAiNGOReaderWriter::_write_MAiNGO_variables(std::ofstream &outputFile)
{

    outputFile << "//////////////////////////////////////////////////////////////////////////\n";
    outputFile << "// function for providing optimization variable data to the Branch-and-Bound solver\n";
    outputFile << "std::vector<maingo::OptimizationVariable>\n";
    outputFile << "Model::get_variables()\n";
    outputFile << "{\n";
    outputFile << "\n";
    outputFile << "    std::vector<maingo::OptimizationVariable> variables;\n";
    outputFile << "    // Required: Define optimization variables by specifying lower bound, upper bound (, optionally variable type, branching priority and a name)\n";

    // Continuous variables
    outputFile << "    // Continuous variables\n";

    for (unsigned int i = 0; i < _ncontVar; i++) {
        outputFile << "    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(" << std::setprecision(16) << _contVariables[i].lowerBound << ","
                   << _contVariables[i].upperBound << "), maingo::VT_CONTINUOUS, \"" << std::setprecision(16) << _contVariables[i].name << "\"));\n";
    }
    // Binary variables
    outputFile << "    // Binary variables\n";
    for (unsigned int i = 0; i < _nbinVar; i++) {
        outputFile << "    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(" << std::setprecision(16) << _binVariables[i].lowerBound << ","
                   << _binVariables[i].upperBound << "), maingo::VT_BINARY, \"" << std::setprecision(16) << _binVariables[i].name << "\"));\n";
    }

    // Integer variables
    outputFile << "    // Integer variables\n";
    for (unsigned int i = 0; i < _nintVar; i++) {
        outputFile << "    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(" << std::setprecision(16) << _intVariables[i].lowerBound << ","
                   << _intVariables[i].upperBound << "), maingo::VT_INTEGER, \"" << std::setprecision(16) << _intVariables[i].name << "\"));\n";
    }

    outputFile << "\n";
    outputFile << "    return variables;\n";
    outputFile << "}\n";
    outputFile << "\n";
    outputFile << "\n";
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for writing initial point to problem.h
void
MAiNGOReaderWriter::_write_MAiNGO_initial_point(std::ofstream &outputFile)
{

    outputFile << "//////////////////////////////////////////////////////////////////////////\n";
    outputFile << "// function for providing initial point data to the Branch-and-Bound solver\n";
    outputFile << "std::vector<double>\n";
    outputFile << "Model::get_initial_point()\n";
    outputFile << "{\n";
    outputFile << "\n";
    outputFile << "    // Here you can provide an initial point for the local search\n";
    outputFile << "    std::vector<double> initialPoint;\n";

    outputFile << "    // Continuous variables\n";
    for (unsigned int i = 0; i < _initialPointCont.size(); i++) {
        outputFile << "    initialPoint.push_back(" << _initialPointCont[i] << ");\n";
    }

    outputFile << "    // Binary variables\n";
    for (unsigned int i = 0; i < _initialPointBin.size(); i++) {
        outputFile << "    initialPoint.push_back(" << _initialPointBin[i] << ");\n";
    }

    outputFile << "    // Integer variables\n";
    for (unsigned int i = 0; i < _initialPointInt.size(); i++) {
        outputFile << "    initialPoint.push_back(" << _initialPointInt[i] << ");\n";
    }

    outputFile << "\n";
    outputFile << "    return initialPoint;\n";
    outputFile << "}\n";
    outputFile << "\n";
    outputFile << "\n";
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for writing the constructor to problem.h
void
MAiNGOReaderWriter::_write_MAiNGO_constructor(std::ofstream &outputFile)
{
    outputFile << "//////////////////////////////////////////////////////////////////////////\n";
    outputFile << "// constructor for the model\n";
    outputFile << "Model::Model()\n";
    outputFile << "{\n";
    outputFile << "\n";
    outputFile << "    // Initialize data if necessary:\n";
    outputFile << "\n";
    outputFile << "}\n";
    outputFile << "\n";
    outputFile << "\n";
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for inserting line breaks in constraints to make the lines in problem.h not extremely long
std::string
MAiNGOReaderWriter::_insert_line_breaks_in_string(const std::string &str, const std::size_t numberWhitespaces)
{

    std::size_t charNr = 140;
    std::string res     = str;
    while (charNr < res.length()) {
        while (res[charNr] != ' ' && res[charNr] != '+' && res[charNr] != '*' && res[charNr] != '/') {
            charNr++;
            if (charNr == res.length() || res[charNr] == ';') {
                break;
            }
        }
        std::string newLine = "\n" + std::string(numberWhitespaces, ' ');
        res.insert(charNr, newLine);
        charNr += 140 + numberWhitespaces;
    }
    return res;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for writing the evaluation to problem.h
void
MAiNGOReaderWriter::_write_MAiNGO_evaluate(std::ofstream &outputFile)
{

    outputFile << "//////////////////////////////////////////////////////////////////////////\n";
    outputFile << "// Evaluate the model\n";
    outputFile << "maingo::EvaluationContainer\n";
    outputFile << "Model::evaluate(const std::vector<Var> &optVars)\n";
    outputFile << "{\n";
    outputFile << "\n";
    outputFile << "    maingo::EvaluationContainer result; /*!< Variable holding the actual result consisting of an objective, inequalities, equalities, relaxation only inequalities and relaxation only equalities */\n";
    outputFile << "\n";

    // Write variables
    unsigned int arrayCounter = 0;
    outputFile << "    // Rename  inputs\n";
    outputFile << "    // Continuous variables\n";
    outputFile << "    unsigned int cVar = 0;\n";
    for (unsigned int i = 0; i < _ncontVar; i++) {
        outputFile << "    Var " << _contVariables[i].name << " = optVars[cVar]; cVar++;\n";
        arrayCounter++;
    }
    outputFile << "    // Binary variables\n";
    outputFile << "    unsigned int bVar = cVar;\n";
    for (unsigned int i = 0; i < _nbinVar; i++) {
        outputFile << "    Var " << _binVariables[i].name << " = optVars[bVar]; bVar++;\n";
        arrayCounter++;
    }
    outputFile << "    // Integer variables\n";
    outputFile << "    unsigned int iVar = bVar;\n";
    for (unsigned int i = 0; i < _nintVar; i++) {
        outputFile << "    Var " << _intVariables[i].name << " = optVars[iVar]; iVar++;\n";
        arrayCounter++;
    }
    outputFile << "\n";

    // Write objective
    outputFile << "    // Objective function\n";
    std::string str = "    result.objective = ";
    outputFile << str << _insert_line_breaks_in_string(_objFunction, str.length()) << ";\n";
    outputFile << "\n";

    // Write inequalities
    outputFile << "    // Inequalities (<=0)\n";
    for (unsigned int i = 0; i < _nlineq; i++) {
        str = "    result.ineq.push_back( ";
        outputFile << str << _insert_line_breaks_in_string(_lineqConstraints[i].lhs, str.length());
        if (_lineqConstraints[i].rhs.compare("0") != 0) {
            outputFile << " - ( " << _insert_line_breaks_in_string(_lineqConstraints[i].rhs, str.length()) << " )";
        }
        outputFile << ", \"" << _lineqConstraints[i].name << "\" );\n";
    }
    for (unsigned int i = 0; i < _ngineq; i++) {
        str = "    result.ineq.push_back( ";
        outputFile << "    result.ineq.push_back( -( " << _insert_line_breaks_in_string(_gineqConstraints[i].lhs, str.length()) << " )";
        if (_gineqConstraints[i].rhs.compare("0") != 0) {
            outputFile << " + ( " << _insert_line_breaks_in_string(_gineqConstraints[i].rhs, str.length()) << " )";
        }
        outputFile << ", \"" << _gineqConstraints[i].name << "\" );\n";
    }
    outputFile << "\n";

    // Write equalities
    outputFile << "    // Equalities (=0)\n";
    for (unsigned int i = 0; i < _neq; i++) {
        str = "    result.eq.push_back( ";
        outputFile << "    result.eq.push_back( " << _insert_line_breaks_in_string(_eqConstraints[i].lhs, str.length());
        if (_eqConstraints[i].rhs.compare("0") != 0) {
            outputFile << " - ( " << _insert_line_breaks_in_string(_eqConstraints[i].rhs, str.length()) << " )";
        }
        outputFile << ", \"" << _eqConstraints[i].name << "\" );\n";
    }

    outputFile << "\n";
    outputFile << "    // Relaxation only inequalities (<=0):\n";
    outputFile << "\n";
    outputFile << "    // Relaxation only equalities (=0):\n";
    outputFile << "\n";
    outputFile << "    return result;\n";
    outputFile << "}\n";
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for writing ALE problem.txt file
void
MAiNGOReaderWriter::write_ALE_problem_file(const std::string &problemFileName, const std::string &dictFileName)
{

    _dict.clear();
    std::ifstream dictFile;
    dictFile.open(dictFileName);
    if (dictFile.is_open()) {
        std::string line;
        while (std::getline(dictFile, line)) {    // Read file line by line
            const auto strEnd = line.find_last_not_of(" ");
            line              = line.substr(0, strEnd + 1);
            _dict.push_back(line);
        }
        _dict.erase(_dict.begin(), _dict.begin() + 15);
        std::cout << "Using dict file " << dictFileName << " to rename variables and equations in the ALE problem file." << std::endl;
    }
    else {    // File not found
        std::cout << "Warning: Could not open GAMS dictionary file " << dictFileName << " while writing ALE problem file.\nProceeding with default variable and constraint names." << std::endl;
    }
    dictFile.close();

    // Make temporary copies of the variables, since we will rename them
    std::vector<OptimizationVariable> tmpContVariables = _contVariables;
    std::vector<OptimizationVariable> tmpBinVariables  = _binVariables;
    std::vector<OptimizationVariable> tmpIntVariables  = _intVariables;
    std::vector<Constraint> tmpEqConstraints           = _eqConstraints;
    std::vector<Constraint> tmpGineqConstraints        = _gineqConstraints;
    std::vector<Constraint> tmpLineqConstraints        = _lineqConstraints;
    _rename_variables_and_constraints();

    std::string outFileName = problemFileName + ".txt";
    std::ofstream outFile(outFileName);

    outFile << "# This file has been automatically generated by the MAiNGO Reader Writer utility.\n\n";

    _write_ALE_variables(outFile);

    _write_ALE_initial_point(outFile);

    _write_ALE_functions(outFile);

    outFile.close();

    // Restore original names
    _contVariables    = tmpContVariables;
    _binVariables     = tmpBinVariables;
    _intVariables     = tmpIntVariables;
    _eqConstraints    = tmpEqConstraints;
    _gineqConstraints = tmpGineqConstraints;
    _lineqConstraints = tmpLineqConstraints;

    std::cout << "ALE problem file written successfully." << std::endl;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for writing variables to problem.h
void
MAiNGOReaderWriter::_write_ALE_variables(std::ofstream &outputFile)
{
    outputFile << "definitions:\n";
    if (_ncontVar) {
        // Continuous variables
        outputFile << "# Continuous variables\n";
        for (unsigned int i = 0; i < _ncontVar; i++) {
            outputFile << "real " << _contVariables[i].name << " in [" << std::setprecision(16) << _contVariables[i].lowerBound << ","
                       << std::setprecision(16) << _contVariables[i].upperBound << "];\n";
        }
        outputFile << "\n";
    }
    if (_nbinVar) {
        // Binary variables
        outputFile << "# Binary variables\n";
        for (unsigned int i = 0; i < _nbinVar; i++) {
            outputFile << "binary " << _binVariables[i].name << ";\n";
        }
        outputFile << "\n";
    }
    if (_nintVar) {
        // Integer variables
        outputFile << "# Integer variables\n";
        for (unsigned int i = 0; i < _nintVar; i++) {
            outputFile << "integer " << _intVariables[i].name << " in [" << _intVariables[i].lowerBound << ","
                       << _intVariables[i].upperBound << "];\n";
        }
        outputFile << "\n";
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
// function for writing initial point to problem.h
void
MAiNGOReaderWriter::_write_ALE_initial_point(std::ofstream &outputFile)
{

    outputFile << "definitions:\n";
    outputFile << "# Initial point\n";
    outputFile << "# Continuous variables\n";
    for (unsigned int i = 0; i < _initialPointCont.size(); i++) {
        outputFile << _contVariables[i].name << ".init <- " << std::setprecision(16) << _initialPointCont[i] << ";\n";
    }

    outputFile << "# Binary variables\n";
    for (unsigned int i = 0; i < _initialPointBin.size(); i++) {
        outputFile << _binVariables[i].name << ".init <- " << std::setprecision(16) << _initialPointBin[i] << ";\n";
    }

    outputFile << "# Integer variables\n";
    for (unsigned int i = 0; i < _initialPointInt.size(); i++) {
        outputFile << _intVariables[i].name << ".init <- " << std::setprecision(16) << _initialPointInt[i] << ";\n";
    }

    outputFile << "\n";
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for writing the evaluation to problem.h
void
MAiNGOReaderWriter::_write_ALE_functions(std::ofstream &outputFile)
{

    outputFile << "constraints:\n";
    // Write inequalities
    if (_nlineq) {
        outputFile << "# Inequalities (<=0)\n";
        for (unsigned int i = 0; i < _nlineq; i++) {
            outputFile << _insert_line_breaks_in_string(_lineqConstraints[i].lhs, 0);
            outputFile << " <= " << _insert_line_breaks_in_string(_lineqConstraints[i].rhs, 0) << " \"" << _lineqConstraints[i].name << "\";\n";
        }
        outputFile << "\n";
    }
    if (_ngineq > 0) {
        outputFile << "# Inequalities (>=0)\n";
        for (unsigned int i = 0; i < _ngineq; i++) {
            outputFile << _insert_line_breaks_in_string(_gineqConstraints[i].lhs, 0);
            outputFile << " >= " << _insert_line_breaks_in_string(_gineqConstraints[i].rhs, 0) << " \"" << _gineqConstraints[i].name << "\";\n";
        }
        outputFile << "\n";
    }

    // Write equalities
    if (_neq > 0) {
        outputFile << "# Equalities (=0)\n";
        for (unsigned int i = 0; i < _neq; i++) {
            outputFile << _insert_line_breaks_in_string(_eqConstraints[i].lhs, 0);
            outputFile << " = " << _insert_line_breaks_in_string(_eqConstraints[i].rhs, 0) << " \"" << _eqConstraints[i].name << "\";\n";
        }
        outputFile << "\n";
    }

    outputFile << "objective:\n";
    // Write objective
    outputFile << "# Objective function\n";
    outputFile << _insert_line_breaks_in_string(_objFunction, 0) << ";";
}


////////////////////////////////////////////////////////////////////////////////////////////
// sets the correct number of linearization points depending on the LBP_linpoints setting
void
MAiNGOReaderWriter::read_MAiNGO_log_file(const std::string &maingoFileName)
{

    _maingoFileName = maingoFileName;
    _file.clear();
    std::ifstream inFile;
    inFile.open(_maingoFileName);
    if (inFile.is_open()) {
        std::string line;
        while (std::getline(inFile, line)) {    // Read file line by line
            _file.push_back(line);
        }
    }
    else {    // File not found
        std::cout << "ERROR: Could not open file " << _maingoFileName << ". Terminating. " << std::endl;
        inFile.close();
        exit(-1);
    }
    inFile.close();

    _check_for_MAiNGO();

    _remove_bab();

    _check_feasibility();

    _read_statistics();

    std::cout << "MAiNGO log file " << _maingoFileName << " read successfully." << std::endl;
}


////////////////////////////////////////////////////////////////////////////////////////////
// checks whether the provided log file is a MAiNGO file (this check is not rigorous)
void
MAiNGOReaderWriter::_check_for_MAiNGO()
{

    std::size_t posMAiNGO;
    bool foundMAiNGO = false;
    for (std::size_t i = 0; i < _file.size(); i++) {
        posMAiNGO = _file[i].find("You are using MAiNGO v");    // This check is not rigorous
        if (posMAiNGO != std::string::npos) {
            foundMAiNGO = true;
            break;
        }
    }

    if (!foundMAiNGO) {
        // The provided file is not a MAiNGO log file or the log file is too old
        std::cout << "ERROR: Provided file " << _maingoFileName << " is not a MAiNGO log file or the log file is too old. Terminating. " << std::endl;
        exit(-1);
    }
    // Else it is a MAiNGO log file and we proceed
}


////////////////////////////////////////////////////////////////////////////////////////////
// function for removing the B&B iterations from the _file vector holding the MAiNGO log file lines.
void
MAiNGOReaderWriter::_remove_bab()
{

    std::size_t start = 0;
    std::size_t end   = 0;
    for (; start < _file.size(); start++) {
        if (_file[start].find("Entering branch-and-bound loop:") != std::string::npos) {
            break;
        }
    }
    end = start;
    for (; end < _file.size(); end++) {
        if (_file[end].find("Done.") != std::string::npos || _file[end].find("*****") != std::string::npos) {
            break;
        }
    }
    if (start < _file.size()) {
        _file.erase(_file.begin() + start, _file.begin() + end + 1);
    }
}


////////////////////////////////////////////////////////////////////////////////////////////
// function for checking whether the problem provided in MAiNGO log is feasible
void
MAiNGOReaderWriter::_check_feasibility()
{

    std::size_t posInfeas;
    _feasibleM = true;
    for (std::size_t i = 0; i < _file.size(); i++) {
        posInfeas = _file[i].find("Problem is infeasible");
        if (posInfeas != std::string::npos) {
            _feasibleM = false;
            break;
        }
    }
}


////////////////////////////////////////////////////////////////////////////////////////////
// function for reading the actual statistics from a MAiNGO log file
void
MAiNGOReaderWriter::_read_statistics()
{

    std::size_t pos;
    for (std::size_t i = 0; i < _file.size(); i++) {
        // Variables
        pos = _file[i].find("Variables");
        if (pos != std::string::npos) {
            std::size_t posEq = _file[i].find("= ");
            _nvarM            = std::stoi(_file[i].substr(posEq + 2, _file[i].length()));
            continue;
        }
        // Equality constraints
        pos = _file[i].find("Equality constraints  ");
        if (pos != std::string::npos) {
            std::size_t posEq = _file[i].find("= ");
            _neqM             = std::stoi(_file[i].substr(posEq + 2, _file[i].length()));
            continue;
        }
        // Inequality constraints
        pos = _file[i].find("Inequality constraints  ");
        if (pos != std::string::npos) {
            std::size_t posEq = _file[i].find("= ");
            _nineqM           = std::stoi(_file[i].substr(posEq + 2, _file[i].length()));
            continue;
        }
        // Equality constraints (relaxation only)
        pos = _file[i].find("Equality constraints (relaxation only)");
        if (pos != std::string::npos) {
            std::size_t posEq   = _file[i].find("= ");
            _neqRelaxationOnlyM = std::stoi(_file[i].substr(posEq + 2, _file[i].length()));
            continue;
        }
        // Inequality constraints (relaxation only)
        pos = _file[i].find("Inequality constraints (relaxation only)");
        if (pos != std::string::npos) {
            std::size_t posEq     = _file[i].find("= ");
            _nineqRelaxationOnlyM = std::stoi(_file[i].substr(posEq + 2, _file[i].length()));
            continue;
        }
        if (_feasibleM) {
            // UBD solved
            pos = _file[i].find("UBD problems solved");
            if (pos != std::string::npos) {
                std::size_t posEq   = _file[i].find("= ");
                _ubdProblemsSolvedM = std::stoi(_file[i].substr(posEq + 2, _file[i].length()));
                continue;
            }
            // LBD solved
            pos = _file[i].find("LBD problems solved");
            if (pos != std::string::npos) {
                std::size_t posEq   = _file[i].find("= ");
                _lbdProblemsSolvedM = std::stoi(_file[i].substr(posEq + 2, _file[i].length()));
                continue;
            }
            // Number of iterations
            pos = _file[i].find("number of iterations");
            if (pos != std::string::npos) {
                std::size_t posEq = _file[i].find("= ");
                _niterationsM     = std::stoi(_file[i].substr(posEq + 2, _file[i].length()));
                continue;
            }
            // Max number of nodes in memory
            pos = _file[i].find("number of nodes in memory");
            if (pos != std::string::npos) {
                std::size_t posEq = _file[i].find("= ");
                _nnodesInMemoryM  = std::stoi(_file[i].substr(posEq + 2, _file[i].length()));
                continue;
            }
            // First found at
            pos = _file[i].find("First found at ");
            if (pos != std::string::npos) {
                std::size_t posEq   = _file[i].find("iteration ");
                _bestSolFirstFoundM = std::stoi(_file[i].substr(posEq + 10, _file[i].length()));
                continue;
            }
            // Final lbd
            pos = _file[i].find("Final LBD");
            if (pos != std::string::npos) {
                std::size_t posEq = _file[i].find("= ");
                std::cout.precision(16);
                _finalLBDM = std::stod(_file[i].substr(posEq + 2, _file[i].length()));
                continue;
            }
            // Final abs gap
            pos = _file[i].find("Final absolute gap");
            if (pos != std::string::npos) {
                std::size_t posEq = _file[i].find("= ");
                std::cout.precision(16);
                _finalAbsGapM = std::stod(_file[i].substr(posEq + 2, _file[i].length()));
                continue;
            }
            // Final rel gap
            pos = _file[i].find("Final relative gap");
            if (pos != std::string::npos) {
                std::size_t posEq = _file[i].find("= ");
                std::cout.precision(16);
                _finalRelGapM = std::stod(_file[i].substr(posEq + 2, _file[i].length()));
            }
            // Final objective value
            pos = _file[i].find("Objective value");
            if (pos != std::string::npos) {
                std::size_t posEq = _file[i].find("= ");
                std::cout.precision(16);
                _objValM = std::stod(_file[i].substr(posEq + 2, _file[i].length()));
                continue;
            }
            // Final lbd
            pos = _file[i].find("Final LBD");
            if (pos != std::string::npos) {
                std::size_t posEq = _file[i].find("= ");
                std::cout.precision(16);
                _finalLBDM = std::stod(_file[i].substr(posEq + 2, _file[i].length()));
                continue;
            }
            // Solution point
            pos = _file[i].find("Solution point:");
            if (pos != std::string::npos) {
                for (std::size_t j = i + 1; j < i + 1 + _nvarM; j++) {
                    std::size_t posEq    = _file[j].find(" = ");
                    std::string variable = _file[j].substr(0, posEq);
                    while (variable[0] == ' ') {
                        variable.erase(variable.begin());
                    }
                    std::cout.precision(16);
                    double value = 0;
                    if (_file[j].find("[", posEq + 3) != std::string::npos) {    // If it is a free variable, simply return the mid point
                        std::size_t posBracketLeft  = _file[j].find("[", posEq + 3);
                        std::size_t posComma        = _file[j].find(",", posEq + 3);
                        std::size_t posBracketRight = _file[j].find("]", posEq + 3);
                        std::string lowerBound      = _file[j].substr(posBracketLeft + 1, posComma - posBracketLeft - 1);
                        std::string upperBound      = _file[j].substr(posComma + 1, posBracketRight - posComma - 1);
                        value                       = (std::stod(upperBound) - std::stod(lowerBound)) / 2.;
                    }
                    else if (_file[j].find("{", posEq + 3) != std::string::npos) {
                        std::size_t posBracketLeft  = _file[j].find("{", posEq + 3);
                        std::size_t posComma        = _file[j].find(",", posEq + 3);
                        std::size_t posBracketRight = _file[j].find("}", posEq + 3);
                        std::string lowerBound      = _file[j].substr(posBracketLeft + 1, posComma - posBracketLeft - 1);
                        std::string upperBound      = _file[j].substr(posComma + 1, posBracketRight - posComma - 1);
                        value                       = std::floor((std::stod(upperBound) - std::stod(lowerBound)) / 2.);
                    }
                    else {
                        value = std::stod(_file[j].substr(posEq + 3, _file[j].length()));
                    }
                    _solutionPoint.push_back(std::make_pair(variable, value));
                }
                i += _nvarM + 1;
            }
            // Additional output
            pos = _file[i].find("Additional Model outputs:");
            if (pos != std::string::npos) {
                _additionalOutput.clear();
                i++;
                std::size_t posEq = _file[i].find(" = ");
                while (posEq != std::string::npos) {
                    std::string variable = _file[i].substr(0, posEq);
                    while (variable[0] == ' ') {
                        variable.erase(variable.begin());
                    }
                    std::cout.precision(16);
                    double value = std::stod(_file[i].substr(posEq + 3, _file[i].length()));
                    _additionalOutput.push_back(std::make_pair(variable, value));
                    i++;
                    posEq = _file[i].find(" = ");
                }
            }
        }
        // Time
        pos = _file[i].find("CPU time: ");
        if (pos != std::string::npos && i > _file.size() - 5) {    // 5 is just to be sure, it could also be 3, since we know that the timing is at the end of the file (for now)
            std::cout.precision(16);
            _cpuTimeNeededM = std::stod(_file[i].substr(pos + std::string("CPU time: ").length(), _file[i].length() - std::string(" seconds (Preprocessing + B&B).").length()));
        }
        pos = _file[i].find("Wall-clock time: ");
        if (pos != std::string::npos && i > _file.size() - 5) {    // 5 is just to be sure, it could also be 3, since we know that the timing is at the end of the file (for now)
            std::cout.precision(16);
            _wallTimeNeededM = std::stod(_file[i].substr(pos + std::string("Wall-clock time: ").length(), _file[i].length() - std::string(" seconds (Preprocessing + B&B).").length()));
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
// function for printing all problem info from MAiNGO log
void
MAiNGOReaderWriter::print_problem_info_MAiNGO_log(std::ostream &outstream)
{

    outstream << "----- Problem Information from MAiNGO log file " << _maingoFileName << " -----" << std::endl
              << std::endl;

    outstream << std::setw(50) << "Total number of variables: " << _nvarM << std::endl;

    outstream << std::endl;

    outstream << std::setw(50) << "Number of equality constraints: " << _neqM << std::endl;
    outstream << std::setw(50) << "Number of <= inequality constraints: " << _nineqM << std::endl;
    outstream << std::setw(50) << "Number of relaxation only equality constraints: " << _neqRelaxationOnlyM << std::endl;
    outstream << std::setw(50) << "Number of relaxation only inequality constraints: " << _nineqRelaxationOnlyM << std::endl;

    outstream << std::endl;

    outstream << std::setw(50) << "UBD problem solved: " << _ubdProblemsSolvedM << std::endl;
    outstream << std::setw(50) << "LBD problem solved: " << _lbdProblemsSolvedM << std::endl;
    outstream << std::setw(50) << "Number of iterations: " << _niterationsM << std::endl;
    outstream << std::setw(50) << "Maximum number of nodes in memory: " << _nnodesInMemoryM << std::endl;
    outstream << std::setw(50) << "Best solution first found in node: " << _bestSolFirstFoundM << std::endl;

    outstream << std::endl;

    outstream << std::setw(50) << "Final LBD: " << _finalLBDM << std::endl;
    outstream << std::setw(50) << "Final abs gap: " << _finalAbsGapM << std::endl;
    outstream << std::setw(50) << "Final rel gap: " << _finalRelGapM << std::endl;
    outstream << std::setw(50) << "Final objective value: " << _objValM << std::endl;
    outstream << std::setw(50) << "Final LBD: " << _finalLBDM << std::endl;
    outstream << std::setw(50) << "CPU time needed: " << _cpuTimeNeededM << std::endl;
    outstream << std::setw(50) << "Wall time needed: " << _wallTimeNeededM << std::endl;

    if (_feasibleM) {
        outstream << std::setw(50) << "Problem is feasible." << std::endl;
    }
    else {
        outstream << std::setw(50) << "Problem is infeasible." << std::endl;
    }
    outstream << "---------------------------------------------------------------------" << std::endl;
}