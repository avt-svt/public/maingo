/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#pragma once

#include <iostream>
#include <string>
#include <vector>

namespace maingo {

namespace readerWriter {

/**
* @enum VT
* @brief Enum for representing the Variable Type of an optimization variable read in from a GAMS convert file
*/
enum VT {
    VT_CONTINUOUS = 0, /*!< Continuous (i.e., real) variable, this is the default if no VT is specified */
    VT_BINARY,         /*!< Binary variable */
    VT_INTEGER,        /*!< Integer variable */
    VT_FIXED           /*!< Fixed variable */
};

/**
* @enum CT
* @brief Enum for representing the Constraint Type of a constraint read in from a GAMS convert file
*/
enum CT {
    CT_EQ = 0, /*!< Equality constraint */
    CT_GINEQ,  /*!< >= inequality constraint */
    CT_LINEQ   /*!< <= inequality constraint */
};

/**
* @struct OptimizationVariable
* @brief Auxiliary struct for representing an optimization variable. Mostly copied from MAiNGOutils.h
*/
struct OptimizationVariable {
    double lowerBound; /*!< Lower interval bound of the optimization variable */
    double upperBound; /*!< Upper interval bound of the optimization variable */
    VT variableType;   /*!< Type of the variable */
    std::string name;  /*!< Name of the variable */
};

/**
* @struct Constraint
* @brief Auxiliary struct for representing a constraint.
*/
struct Constraint {
    CT constraintType; /*!< Type of the constraint */
    std::string lhs;   /*!< Left hand side of the constraint */
    std::string rhs;   /*!< Right hand side of the constraint */
    std::string name;  /*!< Name of the constraint */
};


/**
* @class MAiNGOReaderWriter
* @brief Class for reading in GAMS convert files and writing problem.h files out of it. Moreover, it can read in bab.log files.
*		 It is not the fastest implementation possible but still pretty good, e.g., it is not needed to go through the GAMS file multiple times if it is known that all convert files look the same.
*		 This does not have to be true, e.g., the gams file could have been manipulated! Thus, reading the file more than once is the safest way, even if it's not the fastest one.
*/
class MAiNGOReaderWriter {
  public:
    /**
		* @brief Default constructor
		*/
    MAiNGOReaderWriter();

    /**
		* @brief Destructor
		*/
    ~MAiNGOReaderWriter(){};

    /**
		* @brief Function for reading in a GAMS convert file
		*
		* @param[in] gamsFileName name of input GAMS convert file
		*/
    void read_GAMS_convert_file(const std::string &gamsFileName = "gams.gms");

    /**
		* @brief Function for writing a MAiNGO problem.h file out of an already read in gams convert file
		*
		* @param[in] problemFileName is the name of the output problem file
		* @param[in] dictFileName is the name of a GAMS dictionary file which shall be used to rename variables and constraints
		*/
    void write_MAiNGO_problem_file(const std::string &problemFileName = "problem_gams", const std::string &dictFileName = "dict.txt");

    /**
		* @brief Function for writing an ALE problem.txt file out of an already read in gams convert file
		*
		* @param[in] problemFileName is the name of the output problem file
		* @param[in] dictFileName is the name of a GAMS dictionary file which shall be used to rename variables and constraints
		*/
    void write_ALE_problem_file(const std::string &problemFileName = "problem_gams", const std::string &dictFileName = "dict.txt");

    /**
		* @brief Function for printing problem info given in a previously read GAMS file
		*
		* @param[in,out] outstream is the stream where to print
		*/
    void print_problem_info_GAMS_file(std::ostream &outstream);

    /**
		* @brief Function for reading in all GAMS files in folder input and writing MAiNGO problem files to folder output
		*
		* @param[in] input is the input path, where the GAMS files are
		* @param[in] output is the output path, where the MAiNGO problem files shall be written
		*/
    void convert_GAMS_folder_to_MAiNGO_problem(const std::string &input, const std::string &output);

    /**
		* @brief Function for reading in all GAMS files in folder input and writing ALE problem files to folder output
		*
		* @param[in] input is the input path, where the GAMS files are
		* @param[in] output is the output path, where the ALE problem files shall be written
		*/
    void convert_GAMS_folder_to_ALE_problem(const std::string &input, const std::string &output);

    /**
		* @brief Function for printing problem info given in a previously read GAMS file
		*
		* @param[in] outstream is the stream where to print
		*/
    void print_problem_info_MAiNGO_log(std::ostream &outstream);

    /**
		* @brief Function for reading in a MAiNGO log file
		*
		* @param[in] maingoFileName is the name of input MAiNGO log file
		*/
    void read_MAiNGO_log_file(const std::string &maingoFileName = "bab.log");

    /**
		* @brief Function for getting total number of variables given in a previously read GAMS file
		*/
    unsigned int get_number_variables_GAMS_file();

    /**
		* @brief Function for getting number of continuous variables given in a previously read GAMS file
		*/
    unsigned int get_number_cont_variables_GAMS_file();

    /**
		* @brief Function for getting number of binary variables given in a previously read GAMS file
		*/
    unsigned int get_number_bin_variables_GAMS_file();

    /**
		* @brief Function for getting number of integer variables given in a previously read GAMS file
		*/
    unsigned int get_number_int_variables_GAMS_file();

    /**
		* @brief Function for getting total number of constraints given in a previously read GAMS file
		*/
    unsigned int get_number_constraints_GAMS_file();

    /**
		* @brief Function for getting number of equality constraints given in a previously read GAMS file
		*/
    unsigned int get_number_eq_constraints_GAMS_file();

    /**
		* @brief Function for getting number of >= inequality constraints given in a previously read GAMS file
		*/
    unsigned int get_number_gineq_constraints_GAMS_file();

    /**
		* @brief Function for getting number of <= inequality constraints given in a previously read GAMS file
		*/
    unsigned int get_number_lineq_constraints_GAMS_file();

    /**
		* @brief Function for getting number of fixed variables given in a previously read GAMS file
		*/
    unsigned int get_number_fixed_variables_GAMS_file();

    /**
		* @brief Function for getting the number of the objective variable given in a previously read GAMS file
		*/
    unsigned int get_obj_variable_number_GAMS_file();

    /**
		* @brief Function for getting the name of the objective variable given in a previously read GAMS file
		*/
    std::string get_obj_variable_name_GAMS_file();

    /**
		* @brief Function for getting _minimizing given in a previously read GAMS file
		*/
    bool get_minimizing_GAMS_file();

    /**
		* @brief Function for getting the number of variables read in from MAiNGO log
		*/
    unsigned int get_number_variables_MAiNGO_log();

    /**
		* @brief Function for getting the number of equalities read in from MAiNGO log
		*/
    unsigned int get_number_equalities_MAiNGO_log();

    /**
		* @brief Function for getting the number of inequalities read in from MAiNGO log
		*/
    unsigned int get_number_inequalities_MAiNGO_log();

    /**
		* @brief Function for getting the number of relaxation only equalities read in from MAiNGO log
		*/
    unsigned int get_number_rel_only_eq_MAiNGO_log();

    /**
		* @brief Function for getting the number of relaxation only inequalities read in from MAiNGO log
		*/
    unsigned int get_number_rel_only_ineq_MAiNGO_log();

    /**
		* @brief Function for getting the number of solved ubd problems read in from MAiNGO log
		*/
    unsigned int get_number_ubd_solved_MAiNGO_log();

    /**
		* @brief Function for getting the number of solved lbd problems read in from MAiNGO log
		*/
    unsigned int get_number_lbd_solved_MAiNGO_log();

    /**
		* @brief Function for getting the number of iterations read in from MAiNGO log
		*/
    unsigned int get_number_iterations_MAiNGO_log();

    /**
		* @brief Function for getting the number of nodes in memory read in from MAiNGO log
		*/
    unsigned int get_number_nodes_in_memory_MAiNGO_log();

    /**
		* @brief Function for getting the number of node where best solution was first found read in from MAiNGO log
		*/
    unsigned int get_number_best_first_found_MAiNGO_log();

    /**
		* @brief Function for getting the number of final lbd read in from MAiNGO log
		*/
    double get_final_lbd_MAiNGO_log();

    /**
		* @brief Function for getting the number of final abs gap read in from MAiNGO log
		*/
    double get_final_abs_gap_MAiNGO_log();

    /**
		* @brief Function for getting the number of final rel gap read in from MAiNGO log
		*/
    double get_final_rel_gap_MAiNGO_log();

    /**
		* @brief Function for getting the number of final obj value read in from MAiNGO log
		*/
    double get_final_obj_val_MAiNGO_log();

    /**
		* @brief Function for getting the number of CPU time needed read in from MAiNGO log
		*/
    double get_cpu_time_needed_MAiNGO_log();

    /**
		* @brief Function for getting the number of wall time needed read in from MAiNGO log
		*/
    double get_wall_time_needed_MAiNGO_log();

    /**
		* @brief Function for getting if the problem is feasible read in from MAiNGO log
		*/
    bool get_feasibility_MAiNGO_log();

    /**
		* @brief Function for getting the solution point read in from MAiNGO log
		*/
    std::vector<std::pair<std::string, double>> get_solution_point();

    /**
		* @brief Function for getting additional output read in from MAiNGO log
		*/
    std::vector<std::pair<std::string, double>> get_additional_output();

  private:
    /**
		* @brief Function for reading in the given GAMS file and making sure (not rigorously!) it is a GAMS convert file
		*/
    void _read_file_and_check_it_is_gams_convert();

    /**
		* @brief Function for reading and saving general problem info from a GAMS convert file
		*/
    void _extract_problem_info();

    /**
		* @brief Function for reading and saving variable names from a GAMS convert file
		*/
    void _extract_variable_names();

    /**
		* @brief Function for reading and saving variable bounds from a GAMS convert file
		*/
    void _extract_variable_bounds();

    /**
		* @brief Function for reading and saving the initial point
		*/
    void _extract_initial_point();

    /**
		* @brief Function for reading and saving constraints. It also sets the correct objective function.
		*/
    void _extract_constraints();

    /**
		* @brief Function for obtaining the correct vector index of variable with name varName.
		*
		* @param[in] varName is the variableName
		* @return returns the vector index of the variable
		*/
    unsigned int _find_variable(const std::string &varName);

    /**
		* @brief Function for setting the lower bound of a variable
		*
		* @param[in] varName is the variable Name
		* @param[in] lowerBound is the new lower bound
		*/
    void _set_variable_lower_bound(const std::string &varName, const double lowerBound);

    /**
		* @brief Function for setting the upper bound of a variable
		*
		* @param[in] varName is the variable Name
		* @param[in] upperBound is the new upper bound
		*/
    void _set_variable_upper_bound(const std::string &varName, const double upperBound);

    /**
		* @brief Function for setting the initial point of a variable
		*
		* @param[in] varName is the variable Name
		* @param[in] value is the initial point value
		*/
    void _set_variable_initial_point(const std::string &varName, const double value);

    /**
		* @brief Function for renaming power and ** to pow
		*
		* @param[in,out] constraints is the vector holding the constraints
		*/
    void _rename_powers(std::vector<Constraint> &constraints);

    /**
		* @brief Function for renaming ** to pow
		*
		* @param[in,out] str is the given string
		*/
    void _rename_double_asterisk(std::string &str);

    /**
		* @brief Function for renaming log10 to log/log(10)
		*
		* @param[in,out] constraints is the vector holding the constraints
		*/
    void _rename_logs(std::vector<Constraint> &constraints);

    /**
		* @brief Function for converting log10 GAMS function to log(x)/log(10)
		*
		* @param[in,out] str is the given string
		* @param[in] posLog is the position of the log10 within the string str
		*/
    void _add_division_log10(std::string &str, const std::size_t posLog);

    /**
		* @brief Auxiliary function for removing certain characters from a string
		*
		* @param[in,out] str is the string to be modified
		* @param[in] charsToRemove holds a character array of characters to be removed from str
		*/
    void _remove_chars_from_string(std::string &str, const char *charsToRemove);

    /**
		* @brief Auxiliary function for renaming a given variable by its new name in a particular constraint
		*
		* @param[in,out] constraint is either the lhs or rhs of the constraint
		* @param[in] varName is the original variable name
		* @param[in] newName is new variable name
		*/
    void _replace_all_variables_in_constraint(std::string &constraint, const std::string &varName, const std::string &newName);

    /**
		* @brief Function for renaming variables and constraints w.r.t. a pre-read dictionary file
		*/
    void _rename_variables_and_constraints();

    /**
		* @brief Function for writing the MAiNGO model class to problem.h
		*
		* @param[in] outputFile is the file to write to
		* @param[in] problemName is the name of the problem file
		*/
    void _write_MAiNGO_model(std::ofstream &outputFile, const std::string &problemName);

    /**
		* @brief Function for writing MAiNGO variables to problem.h
		*
		* @param[in] outputFile is the file to write to
		*/
    void _write_MAiNGO_variables(std::ofstream &outputFile);

    /**
		* @brief Function for writing the initial point to problem.h
		*
		* @param[in,out] outputFile is the file to write to
		*/
    void _write_MAiNGO_initial_point(std::ofstream &outputFile);

    /**
		* @brief Function for writing the constructor to problem.h
		*
		* @param[in,out] outputFile is the file to write to
		*/
    void _write_MAiNGO_constructor(std::ofstream &outputFile);

    /**
		* @brief Function for inserting line breaks into constraint strings to make the lines in problem.h not extremely long
		*
		* @param[in] str is the string where line breaks will be added
		* @param[in] numberWhitespaces is the number of whitespaces, that are inserted after a new line has been added
		* @return Returns the function string with properly inserted line breaks
		*/
    std::string _insert_line_breaks_in_string(const std::string &str, const size_t numberWhitespaces);

    /**
		* @brief Function for writing the evaluation to problem.h
		*
		* @param[in,out] outputFile is the file to write to
		*/
    void _write_MAiNGO_evaluate(std::ofstream &outputFile);

    /**
		* @brief Function for writing ALE variables to problem.txt
		*
		* @param[in,out] outputFile is the file to write to
		*/
    void _write_ALE_variables(std::ofstream &outputFile);

    /**
		* @brief Function for writing ALE initial point to problem.txt
		*
		* @param[in,out] outputFile is the file to write to
		*/
    void _write_ALE_initial_point(std::ofstream &outputFile);

    /**
		* @brief Function for writing all constraints and the objective to ALE problem.txt
		*
		* @param[in,out] outputFile is the file to write to
		*/
    void _write_ALE_functions(std::ofstream &outputFile);

    /**
		* @brief Function for checking whether the provided MAiNGO log is really a MAiNGO log. The check is not rigorous!
		*/
    void _check_for_MAiNGO();

    /**
		* @brief Function for removing the B&B iterations from the _file vector holding the MAiNGO log file lines.
		*/
    void _remove_bab();

    /**
		* @brief Function for checking whether the problem provided in MAiNGO log is feasible
		*/
    void _check_feasibility();

    /**
		* @brief Function for reading the actual statistics from a MAiNGO log file
		*/
    void _read_statistics();

    /**
		* @name Internal variables for storing information on the GAMS problem
		*/
    /**@{*/
    std::string _gamsFileName;      /*!< Name of GAMS file to read in, default is gams.gms */
    std::vector<std::string> _file; /*!< Vector holding all lines of a given GAMS file */
    std::vector<std::string> _dict; /*!< Vector holding all lines of a given GAMS dictionary file */

    unsigned int _nvar;     /*!< Total number of variables in the GAMS file */
    unsigned int _ncontVar; /*!< Number of continuous variables in the GAMS file */
    unsigned int _nbinVar;  /*!< Number of binary variables in the GAMS file */
    unsigned int _nintVar;  /*!< Number of integer variables in the GAMS file */

    unsigned int _ncons;  /*!< Total number of constraints in the GAMS file */
    unsigned int _neq;    /*!< Number of equality constraints in the GAMS file */
    unsigned int _nlineq; /*!< Number of <= inequality constraints in the GAMS file */
    unsigned int _ngineq; /*!< Number of >= inequality constraints in the GAMS file */

    unsigned int _nfixedVar;  /*!< Number of fixed variables in the GAMS file */
    unsigned int _objNr;      /*!< Number of objective variable in the GAMS file */
    std::string _objName;     /*!< Name of objective variable in the GAMS file */
    std::string _objFunction; /*!< Final objective function - may be equal to _objName in the GAMS file */
    bool _objSingle;          /*!< If this is true, the objective function is unique in the GAMS file */
    bool _minimizing;         /*!< Tells whether we are minimizing or maximizing in the GAMS file */

    std::vector<OptimizationVariable> _contVariables; /*!< Vector holding continuous variables in the GAMS file */
    std::vector<OptimizationVariable> _binVariables;  /*!< Vector holding binary variables in the GAMS file */
    std::vector<OptimizationVariable> _intVariables;  /*!< Vector holding integer variables in the GAMS file */
    std::vector<Constraint> _constraints;             /*!< Vector holding all constraints in the GAMS file */
    std::vector<Constraint> _eqConstraints;           /*!< Vector holding equality constraints in the GAMS file */
    std::vector<Constraint> _gineqConstraints;        /*!< Vector holding >= inequality constraints in the GAMS file */
    std::vector<Constraint> _lineqConstraints;        /*!< Vector holding <= inequality constraints in the GAMS file */
    std::vector<double> _initialPointCont;            /*!< Vector holding the initial values for continuous variables in the GAMS file */
    std::vector<unsigned int> _initialPointBin;       /*!< Vector holding the initial values for binary variables in the GAMS file */
    std::vector<int> _initialPointInt;                /*!< Vector holding the initial values for integer variables in the GAMS file */
    /**@}*/

    /**
		* @name Internal variables for storing information given in the MAiNGO log, the letter 'M' is used to avoid name collision
		*/
    /**@{*/
    std::string _maingoFileName; /*!< Name of MAiNGO log file to read in, default is bab.log */

    unsigned int _nvarM;                /*!< Total number of variables in the MAiNGO log*/
    unsigned int _neqM;                 /*!< Number of equalities in the MAiNGO log*/
    unsigned int _nineqM;               /*!< Number of inequalities in the MAiNGO log*/
    unsigned int _neqRelaxationOnlyM;   /*!< Number of relaxation only equalities in the MAiNGO log*/
    unsigned int _nineqRelaxationOnlyM; /*!< Number of relaxation only inequalities in the MAiNGO log*/

    unsigned int _ubdProblemsSolvedM; /*!< Number of solved ubd problems in the MAiNGO log*/
    unsigned int _lbdProblemsSolvedM; /*!< Number of solved lbd problems in the MAiNGO log*/
    unsigned int _niterationsM;       /*!< Number of iterations in the MAiNGO log*/
    unsigned int _nnodesInMemoryM;    /*!< Maximal number of nodes in memory in the MAiNGO log*/
    unsigned int _bestSolFirstFoundM; /*!< Number of node where best solution was first found in the MAiNGO log*/

    double _finalLBDM;                                             /*!< Final lbd in the MAiNGO log*/
    double _finalAbsGapM;                                          /*!< Final absolute gap in the MAiNGO log*/
    double _finalRelGapM;                                          /*!< Final relative gap in the MAiNGO log*/
    double _objValM;                                               /*!< Final objective value in the MAiNGO log*/
    double _cpuTimeNeededM;                                        /*!< CPU time needed in the MAiNGO log*/
    double _wallTimeNeededM;                                       /*!< Wall time needed in the MAiNGO log*/
    bool _feasibleM;                                               /*!< Whether the problem is feasible or not in the MAiNGO log*/
    std::vector<std::pair<std::string, double>> _solutionPoint;    /*!< Solution point variable names and values in the MAiNGO log*/
    std::vector<std::pair<std::string, double>> _additionalOutput; /*!< Additional output variable names and values in the MAiNGO log*/
    /**@}*/

    // Prevent use of default copy constructor and copy assignment operator by declaring them private:
    MAiNGOReaderWriter(const MAiNGOReaderWriter &);            /*!< default copy constructor declared private to prevent use */
    MAiNGOReaderWriter &operator=(const MAiNGOReaderWriter &); /*!< default assignment operator declared private to prevent use */
};

}    // namespace readerWriter

}    // namespace maingo