/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#pragma once

/**

@mainpage MAiNGO Reader Writer Manual
@authors Dominik Bongartz, Jaromił Najman, Susanne Sass, Alexander Mitsos
@copyright Process Systems Engineering (AVT.SVT), RWTH Aachen University
@date 12.06.2020

<br>
@section intro_sec Introduction

Welcome to MAiNGO! If you have any issues, concerns, or comments, please communicate them using the "Issues" functionality in GitLab or write an e-mail to MAiNGO@avt.rwth-aachen.de.

<br>
@subsection what_is_sec What is the MAiNGO Reader Writer utility?

The MAiNGO Reader Writer utility is a seperate tool, that can be used to write an ALE problem.txt or a MAiNGO problem.h file out of a GAMS convert file. In order to generate a GAMS convert file, you have to set your solver to
convert and execute the solution process in GAMS. In general, this generates a gams.gms file which then can be used as input for the MAiNGO Reader Writer utility. It is also possible to translate
whole folders of GAMS convert files. If you additionally provide the GAMS dictionary file (<tt>dict.txt</tt>), this tool will rename the default GAMS convert variables to your original ones.

The MAiNGO Reader Writer utility can also be used to read information such as time needed, number of iterations or the final lower bound from MAiNGO log files. This can be useful when one wants
 to automatically process many log files and, e.g., generate tables automatically.

<br>
@subsection compiling Which compiliers are supported?

The MAiNGO Reader Writer utility has been successfully tested for GCC and Visual Studio compilers. <br>
The Intel compiler currently does not support the C++17 <tt>filesystem</tt> package making a compilation of the MAiNGO Reader Writer tool not possible.

<br>
@subsection usage How to use the MAiNGO Reader Writer utility?

After generating and compiling the project, first take a look into the main.cpp. Here you see how to  \ref maingo.readerWriter.MAiNGOReaderWriter.read_GAMS_convert_file "read a GAMS convert file".
You can either specify the path to the GAMS convert file in the main.cpp by replacing the gamsFile string or by directly calling the MAiNGOReaderWriter.exe with an additional command line argument
specifiying the path to your GAMS convert file. You can also \ref maingo.readerWriter.MAiNGOReaderWriter.print_problem_info_GAMS_file "print the information about the currently read GAMS file". Note
that reading in a new GAMS file overrides all internal information about the problem.

After reading the GAMS convert file, you can either \ref maingo.readerWriter.MAiNGOReaderWriter.write_ALE_problem_file "write an ALE problem.txt file" or
\ref maingo.readerWriter.MAiNGOReaderWriter.write_MAiNGO_problem_file "write a MAiNGO problem.h file" out of the lastly read in GAMS convert file. This enables the translation of GAMS convert files
to MAiNGO specific input files. Please note that a direct way of using GAMS convert files as input for MAiNGO represents one of the works in progress.

Please note that it is possible to directly translate multiple GAMS convert files to \ref maingo.readerWriter.MAiNGOReaderWriter.convert_GAMS_folder_to_MAiNGO_problem "MAiNGO problem files" or
\ref maingo.readerWriter.MAiNGOReaderWriter.convert_GAMS_folder_to_ALE_problem "ALE problem files".

Furthermore, the MAiNGO Reader Writer utility can be used to \ref maingo.readerWriter.MAiNGOReaderWriter.read_MAiNGO_log_file "read MAiNGO log files" in order to process the information
further and, e.g., write a table. It is also possible to \ref maingo.readerWriter.MAiNGOReaderWriter.print_problem_info_MAiNGO_log "print the information read from a MAiNGO log file".
*/