/**
* @file ../../output_MAiNGO_problem\problem_CSII_LCOE.h
*
* @brief This file was generated using the MAiNGOReaderWriter class.
*        File contains declaration of the Model object and the problem parsed from a GAMS convert file.
*
* ==============================================================================\n
* © Aachener Verfahrenstechnik-Systemverfahrenstechnik, RWTH Aachen University  \n
* ==============================================================================\n
*
* @author Dominik Bongartz, Jaromil Najman, Susanne Sass, Alexander Mitsos
* @date 6.11.2019
*
*/ 
#pragma once

#include "MAiNGOmodel.h"


using Var = mc::FFVar;    // This allows us to write Var instead of mc::FFVar


/**
* @class Model
* @brief Class defining the actual model implemented by the user 
*
* This class is used by the user to implement the model 
*/
class Model: public maingo::MAiNGOmodel {

    public:
        /**
        * @brief Default constructor 
        */
        Model();

        /**
        * @brief Main function used to evaluate the model and construct a directed acyclic graph
        *
        * @param[in] optVars is the optimization variables vector
        * @param[in] writeAdditionalOutput defines whether to write additional output 
        */  
        maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);

        /**
        * @brief Function for getting optimization variables data
        */
        std::vector<maingo::OptimizationVariable> get_variables();

        /**
        * @brief Function for getting initial point data
        */
        std::vector<double> get_initial_point();

    private:
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{

    std::vector<maingo::OptimizationVariable> variables;
    // Required: Define optimization variables by specifying lower bound, upper bound (, optionally variable type, branching priority and a name)
    // Continuous variables
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1000), maingo::VT_CONTINUOUS, "x1"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x2"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x3"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x4"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x5"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x6"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x7"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05,100), maingo::VT_CONTINUOUS, "x8"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.2,100), maingo::VT_CONTINUOUS, "x9"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05,100), maingo::VT_CONTINUOUS, "x10"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(3,100), maingo::VT_CONTINUOUS, "x11"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05,100), maingo::VT_CONTINUOUS, "x12"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05,100), maingo::VT_CONTINUOUS, "x13"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05,100), maingo::VT_CONTINUOUS, "x14"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05,5), maingo::VT_CONTINUOUS, "x15"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05,3), maingo::VT_CONTINUOUS, "x16"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05,5), maingo::VT_CONTINUOUS, "x17"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05,3), maingo::VT_CONTINUOUS, "x18"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.85,1), maingo::VT_CONTINUOUS, "x19"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.85,1), maingo::VT_CONTINUOUS, "x20"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.5,1), maingo::VT_CONTINUOUS, "x21"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.5,1), maingo::VT_CONTINUOUS, "x22"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,300), maingo::VT_CONTINUOUS, "x23"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.25,325), maingo::VT_CONTINUOUS, "x24"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.25,1500), maingo::VT_CONTINUOUS, "x25"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.25,1500), maingo::VT_CONTINUOUS, "x26"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.25,1500), maingo::VT_CONTINUOUS, "x27"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2480,3200), maingo::VT_CONTINUOUS, "x28"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2480,3750), maingo::VT_CONTINUOUS, "x29"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1240,3200), maingo::VT_CONTINUOUS, "x30"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1240,3200), maingo::VT_CONTINUOUS, "x31"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1240,3200), maingo::VT_CONTINUOUS, "x32"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1240,3200), maingo::VT_CONTINUOUS, "x33"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(4.23,10), maingo::VT_CONTINUOUS, "x34"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2.115,10), maingo::VT_CONTINUOUS, "x35"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2.115,10), maingo::VT_CONTINUOUS, "x36"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x37"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x38"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x39"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x40"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x41"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x42"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x43"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x44"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x45"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x46"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), maingo::VT_CONTINUOUS, "x47"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1500), maingo::VT_CONTINUOUS, "x48"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1500), maingo::VT_CONTINUOUS, "x49"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1500), maingo::VT_CONTINUOUS, "x50"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1500), maingo::VT_CONTINUOUS, "x51"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1500), maingo::VT_CONTINUOUS, "x52"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1500), maingo::VT_CONTINUOUS, "x53"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2480,3200), maingo::VT_CONTINUOUS, "x54"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2480,3200), maingo::VT_CONTINUOUS, "x55"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2480,3200), maingo::VT_CONTINUOUS, "x56"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2480,3200), maingo::VT_CONTINUOUS, "x57"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2480,3200), maingo::VT_CONTINUOUS, "x58"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2480,3200), maingo::VT_CONTINUOUS, "x59"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,3), maingo::VT_CONTINUOUS, "x60"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,3), maingo::VT_CONTINUOUS, "x61"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(4.23,10), maingo::VT_CONTINUOUS, "x62"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(4.23,10), maingo::VT_CONTINUOUS, "x63"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(5,100), maingo::VT_CONTINUOUS, "x64"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.01,0.2), maingo::VT_CONTINUOUS, "x65"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(423,900), maingo::VT_CONTINUOUS, "x66"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(423,900), maingo::VT_CONTINUOUS, "x67"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(423,900), maingo::VT_CONTINUOUS, "x68"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(12400,375000), maingo::VT_CONTINUOUS, "x69"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,12.5), maingo::VT_CONTINUOUS, "x70"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1250), maingo::VT_CONTINUOUS, "x71"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,12.5), maingo::VT_CONTINUOUS, "x72"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1250), maingo::VT_CONTINUOUS, "x73"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,3750), maingo::VT_CONTINUOUS, "x74"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(500,375000), maingo::VT_CONTINUOUS, "x75"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,3750), maingo::VT_CONTINUOUS, "x76"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(500,375000), maingo::VT_CONTINUOUS, "x77"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(200,375000), maingo::VT_CONTINUOUS, "x78"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(10,100000), maingo::VT_CONTINUOUS, "x79"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(10,100000), maingo::VT_CONTINUOUS, "x80"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(10,100000), maingo::VT_CONTINUOUS, "x81"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(10,100000), maingo::VT_CONTINUOUS, "x82"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(15,570), maingo::VT_CONTINUOUS, "x83"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(15,570), maingo::VT_CONTINUOUS, "x84"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(15,570), maingo::VT_CONTINUOUS, "x85"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(15,570), maingo::VT_CONTINUOUS, "x86"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(15,570), maingo::VT_CONTINUOUS, "x87"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(15,570), maingo::VT_CONTINUOUS, "x88"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(15,570), maingo::VT_CONTINUOUS, "x89"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(15,570), maingo::VT_CONTINUOUS, "x90"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(15,570), maingo::VT_CONTINUOUS, "x91"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(15,570), maingo::VT_CONTINUOUS, "x92"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(15,570), maingo::VT_CONTINUOUS, "x93"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(15,570), maingo::VT_CONTINUOUS, "x94"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(10,100000), maingo::VT_CONTINUOUS, "x95"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(10,100000), maingo::VT_CONTINUOUS, "x96"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(10,100000), maingo::VT_CONTINUOUS, "x97"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(10,100000), maingo::VT_CONTINUOUS, "x98"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,100000000), maingo::VT_CONTINUOUS, "x99"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,100000000), maingo::VT_CONTINUOUS, "x100"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,100000000), maingo::VT_CONTINUOUS, "x101"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,100000000), maingo::VT_CONTINUOUS, "x102"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1,100), maingo::VT_CONTINUOUS, "x103"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1,100), maingo::VT_CONTINUOUS, "x104"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1,100), maingo::VT_CONTINUOUS, "x105"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1,100), maingo::VT_CONTINUOUS, "x106"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,100000000), maingo::VT_CONTINUOUS, "x107"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,100000000), maingo::VT_CONTINUOUS, "x108"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,100000000), maingo::VT_CONTINUOUS, "x109"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,100000000), maingo::VT_CONTINUOUS, "x110"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1,1000), maingo::VT_CONTINUOUS, "x111"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,100000000), maingo::VT_CONTINUOUS, "x112"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,10000000), maingo::VT_CONTINUOUS, "x113"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,10000000), maingo::VT_CONTINUOUS, "x114"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,10000000), maingo::VT_CONTINUOUS, "x115"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,500000000), maingo::VT_CONTINUOUS, "x116"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(100,500000000), maingo::VT_CONTINUOUS, "x117"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(70200,445000), maingo::VT_CONTINUOUS, "x118"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.001,1), maingo::VT_CONTINUOUS, "x119"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1000), maingo::VT_CONTINUOUS, "x120"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1000), maingo::VT_CONTINUOUS, "x121"));
    // Binary variables
    // Integer variables

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// function for providing initial point data to the Branch-and-Bound solver
std::vector<double>
Model::get_initial_point()
{

    // Here you can provide an initial point for the local search
    std::vector<double> initialPoint;
    // Continuous variables
    initialPoint.push_back(0);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(0.05);
    initialPoint.push_back(0.2);
    initialPoint.push_back(0.05);
    initialPoint.push_back(3);
    initialPoint.push_back(0.05);
    initialPoint.push_back(0.05);
    initialPoint.push_back(0.05);
    initialPoint.push_back(0.05);
    initialPoint.push_back(0.05);
    initialPoint.push_back(0.05);
    initialPoint.push_back(0.05);
    initialPoint.push_back(0.85);
    initialPoint.push_back(0.85);
    initialPoint.push_back(0.5);
    initialPoint.push_back(0.5);
    initialPoint.push_back(0);
    initialPoint.push_back(0.25);
    initialPoint.push_back(0.25);
    initialPoint.push_back(0.25);
    initialPoint.push_back(0.25);
    initialPoint.push_back(2480);
    initialPoint.push_back(2480);
    initialPoint.push_back(1240);
    initialPoint.push_back(1240);
    initialPoint.push_back(1240);
    initialPoint.push_back(1240);
    initialPoint.push_back(4.23);
    initialPoint.push_back(2.115);
    initialPoint.push_back(2.115);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(300);
    initialPoint.push_back(0);
    initialPoint.push_back(0);
    initialPoint.push_back(0);
    initialPoint.push_back(0);
    initialPoint.push_back(0);
    initialPoint.push_back(0);
    initialPoint.push_back(2480);
    initialPoint.push_back(2480);
    initialPoint.push_back(2480);
    initialPoint.push_back(2480);
    initialPoint.push_back(2480);
    initialPoint.push_back(2480);
    initialPoint.push_back(0);
    initialPoint.push_back(0);
    initialPoint.push_back(4.23);
    initialPoint.push_back(4.23);
    initialPoint.push_back(5);
    initialPoint.push_back(0.01);
    initialPoint.push_back(423);
    initialPoint.push_back(423);
    initialPoint.push_back(423);
    initialPoint.push_back(12400);
    initialPoint.push_back(0);
    initialPoint.push_back(0);
    initialPoint.push_back(0);
    initialPoint.push_back(0);
    initialPoint.push_back(100);
    initialPoint.push_back(500);
    initialPoint.push_back(100);
    initialPoint.push_back(500);
    initialPoint.push_back(200);
    initialPoint.push_back(10);
    initialPoint.push_back(10);
    initialPoint.push_back(10);
    initialPoint.push_back(10);
    initialPoint.push_back(15);
    initialPoint.push_back(15);
    initialPoint.push_back(15);
    initialPoint.push_back(15);
    initialPoint.push_back(15);
    initialPoint.push_back(15);
    initialPoint.push_back(15);
    initialPoint.push_back(15);
    initialPoint.push_back(15);
    initialPoint.push_back(15);
    initialPoint.push_back(15);
    initialPoint.push_back(15);
    initialPoint.push_back(10);
    initialPoint.push_back(10);
    initialPoint.push_back(10);
    initialPoint.push_back(10);
    initialPoint.push_back(100);
    initialPoint.push_back(100);
    initialPoint.push_back(100);
    initialPoint.push_back(100);
    initialPoint.push_back(1);
    initialPoint.push_back(1);
    initialPoint.push_back(1);
    initialPoint.push_back(1);
    initialPoint.push_back(100);
    initialPoint.push_back(100);
    initialPoint.push_back(100);
    initialPoint.push_back(100);
    initialPoint.push_back(1);
    initialPoint.push_back(100);
    initialPoint.push_back(100);
    initialPoint.push_back(100);
    initialPoint.push_back(100);
    initialPoint.push_back(100);
    initialPoint.push_back(100);
    initialPoint.push_back(70200);
    initialPoint.push_back(0.001);
    initialPoint.push_back(0);
    initialPoint.push_back(0);
    // Binary variables
    // Integer variables

    return initialPoint;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model()
{

    // Initialize data if necessary:

}


//////////////////////////////////////////////////////////////////////////
// Evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var> &optVars)
{

    maingo::EvaluationContainer result; /*!< Variable holding the actual result consisting of an objective, inequalities, equalities, relaxation only inequalities and relaxation only equalities */

    // Rename  inputs
    // Continuous variables
    unsigned int cVar = 0;
    Var x1 = optVars[cVar]; cVar++;
    Var x2 = optVars[cVar]; cVar++;
    Var x3 = optVars[cVar]; cVar++;
    Var x4 = optVars[cVar]; cVar++;
    Var x5 = optVars[cVar]; cVar++;
    Var x6 = optVars[cVar]; cVar++;
    Var x7 = optVars[cVar]; cVar++;
    Var x8 = optVars[cVar]; cVar++;
    Var x9 = optVars[cVar]; cVar++;
    Var x10 = optVars[cVar]; cVar++;
    Var x11 = optVars[cVar]; cVar++;
    Var x12 = optVars[cVar]; cVar++;
    Var x13 = optVars[cVar]; cVar++;
    Var x14 = optVars[cVar]; cVar++;
    Var x15 = optVars[cVar]; cVar++;
    Var x16 = optVars[cVar]; cVar++;
    Var x17 = optVars[cVar]; cVar++;
    Var x18 = optVars[cVar]; cVar++;
    Var x19 = optVars[cVar]; cVar++;
    Var x20 = optVars[cVar]; cVar++;
    Var x21 = optVars[cVar]; cVar++;
    Var x22 = optVars[cVar]; cVar++;
    Var x23 = optVars[cVar]; cVar++;
    Var x24 = optVars[cVar]; cVar++;
    Var x25 = optVars[cVar]; cVar++;
    Var x26 = optVars[cVar]; cVar++;
    Var x27 = optVars[cVar]; cVar++;
    Var x28 = optVars[cVar]; cVar++;
    Var x29 = optVars[cVar]; cVar++;
    Var x30 = optVars[cVar]; cVar++;
    Var x31 = optVars[cVar]; cVar++;
    Var x32 = optVars[cVar]; cVar++;
    Var x33 = optVars[cVar]; cVar++;
    Var x34 = optVars[cVar]; cVar++;
    Var x35 = optVars[cVar]; cVar++;
    Var x36 = optVars[cVar]; cVar++;
    Var x37 = optVars[cVar]; cVar++;
    Var x38 = optVars[cVar]; cVar++;
    Var x39 = optVars[cVar]; cVar++;
    Var x40 = optVars[cVar]; cVar++;
    Var x41 = optVars[cVar]; cVar++;
    Var x42 = optVars[cVar]; cVar++;
    Var x43 = optVars[cVar]; cVar++;
    Var x44 = optVars[cVar]; cVar++;
    Var x45 = optVars[cVar]; cVar++;
    Var x46 = optVars[cVar]; cVar++;
    Var x47 = optVars[cVar]; cVar++;
    Var x48 = optVars[cVar]; cVar++;
    Var x49 = optVars[cVar]; cVar++;
    Var x50 = optVars[cVar]; cVar++;
    Var x51 = optVars[cVar]; cVar++;
    Var x52 = optVars[cVar]; cVar++;
    Var x53 = optVars[cVar]; cVar++;
    Var x54 = optVars[cVar]; cVar++;
    Var x55 = optVars[cVar]; cVar++;
    Var x56 = optVars[cVar]; cVar++;
    Var x57 = optVars[cVar]; cVar++;
    Var x58 = optVars[cVar]; cVar++;
    Var x59 = optVars[cVar]; cVar++;
    Var x60 = optVars[cVar]; cVar++;
    Var x61 = optVars[cVar]; cVar++;
    Var x62 = optVars[cVar]; cVar++;
    Var x63 = optVars[cVar]; cVar++;
    Var x64 = optVars[cVar]; cVar++;
    Var x65 = optVars[cVar]; cVar++;
    Var x66 = optVars[cVar]; cVar++;
    Var x67 = optVars[cVar]; cVar++;
    Var x68 = optVars[cVar]; cVar++;
    Var x69 = optVars[cVar]; cVar++;
    Var x70 = optVars[cVar]; cVar++;
    Var x71 = optVars[cVar]; cVar++;
    Var x72 = optVars[cVar]; cVar++;
    Var x73 = optVars[cVar]; cVar++;
    Var x74 = optVars[cVar]; cVar++;
    Var x75 = optVars[cVar]; cVar++;
    Var x76 = optVars[cVar]; cVar++;
    Var x77 = optVars[cVar]; cVar++;
    Var x78 = optVars[cVar]; cVar++;
    Var x79 = optVars[cVar]; cVar++;
    Var x80 = optVars[cVar]; cVar++;
    Var x81 = optVars[cVar]; cVar++;
    Var x82 = optVars[cVar]; cVar++;
    Var x83 = optVars[cVar]; cVar++;
    Var x84 = optVars[cVar]; cVar++;
    Var x85 = optVars[cVar]; cVar++;
    Var x86 = optVars[cVar]; cVar++;
    Var x87 = optVars[cVar]; cVar++;
    Var x88 = optVars[cVar]; cVar++;
    Var x89 = optVars[cVar]; cVar++;
    Var x90 = optVars[cVar]; cVar++;
    Var x91 = optVars[cVar]; cVar++;
    Var x92 = optVars[cVar]; cVar++;
    Var x93 = optVars[cVar]; cVar++;
    Var x94 = optVars[cVar]; cVar++;
    Var x95 = optVars[cVar]; cVar++;
    Var x96 = optVars[cVar]; cVar++;
    Var x97 = optVars[cVar]; cVar++;
    Var x98 = optVars[cVar]; cVar++;
    Var x99 = optVars[cVar]; cVar++;
    Var x100 = optVars[cVar]; cVar++;
    Var x101 = optVars[cVar]; cVar++;
    Var x102 = optVars[cVar]; cVar++;
    Var x103 = optVars[cVar]; cVar++;
    Var x104 = optVars[cVar]; cVar++;
    Var x105 = optVars[cVar]; cVar++;
    Var x106 = optVars[cVar]; cVar++;
    Var x107 = optVars[cVar]; cVar++;
    Var x108 = optVars[cVar]; cVar++;
    Var x109 = optVars[cVar]; cVar++;
    Var x110 = optVars[cVar]; cVar++;
    Var x111 = optVars[cVar]; cVar++;
    Var x112 = optVars[cVar]; cVar++;
    Var x113 = optVars[cVar]; cVar++;
    Var x114 = optVars[cVar]; cVar++;
    Var x115 = optVars[cVar]; cVar++;
    Var x116 = optVars[cVar]; cVar++;
    Var x117 = optVars[cVar]; cVar++;
    Var x118 = optVars[cVar]; cVar++;
    Var x119 = optVars[cVar]; cVar++;
    Var x120 = optVars[cVar]; cVar++;
    Var x121 = optVars[cVar]; cVar++;
    // Binary variables
    unsigned int bVar = cVar;
    // Integer variables
    unsigned int iVar = bVar;

    // Objective function
    result.objective = x1;

    // Inequalities (<=0)
    result.ineq.push_back( -( x29 - x55  ) );
    result.ineq.push_back( -( - x41 + x67  ) + ( 15 ) );
    result.ineq.push_back( -( - x3 + x68  ) + ( 15 ) );
    result.ineq.push_back( -( - x9 + x11  ) );

    // Equalities (=0)
    result.eq.push_back( - 643.748/(3.55959 - (log(x8)/log(10.))) + x37  - ( 198.043 ) );
    result.eq.push_back( - 643.748/(3.55959 - (log(x9)/log(10.))) + x38  - ( 198.043 ) );
    result.eq.push_back( - 643.748/(3.55959 - (log(x10)/log(10.))) + x39  - ( 198.043 ) );
    result.eq.push_back( - 643.748/(3.55959 - (log(x11)/log(10.))) + x40  - ( 198.043 ) );
    result.eq.push_back( - 643.748/(3.55959 - (log(x12)/log(10.))) + x41  - ( 198.043 ) );
    result.eq.push_back( - 643.748/(3.55959 - (log(x13)/log(10.))) + x42  - ( 198.043 ) );
    result.eq.push_back( - 643.748/(3.55959 - (log(x14)/log(10.))) + x43  - ( 198.043 ) );
    result.eq.push_back( - 643.748/(3.55959 - (log(x15)/log(10.))) + x44  - ( 198.043 ) );
    result.eq.push_back( - 643.748/(3.55959 - (log(x16)/log(10.))) + x45  - ( 198.043 ) );
    result.eq.push_back( - 643.748/(3.55959 - (log(x17)/log(10.))) + x46  - ( 198.043 ) );
    result.eq.push_back( - 643.748/(3.55959 - (log(x18)/log(10.))) + x47  - ( 198.043 ) );
    result.eq.push_back( - 0.1*x8 - 4.18*x37 + x48  - ( -1311.817088 ) );
    result.eq.push_back( - 0.1*x10 - 4.18*x39 + x49  - ( -1311.817088 ) );
    result.eq.push_back( - 0.1*x15 - 4.18*x44 + x50  - ( -1311.817088 ) );
    result.eq.push_back( - 0.1*x16 - 4.18*x45 + x51  - ( -1311.817088 ) );
    result.eq.push_back( - 0.1*x17 - 4.18*x46 + x52  - ( -1311.817088 ) );
    result.eq.push_back( - 0.1*x18 - 4.18*x47 + x53  - ( -1311.817088 ) );
    result.eq.push_back( - 2.08*x42 + x54  - ( 1827.230272 ) );
    result.eq.push_back( - 2.08*x43 + x55  - ( 1827.230272 ) );
    result.eq.push_back( - 2.08*x44 + x56  - ( 1827.230272 ) );
    result.eq.push_back( - 2.08*x45 + x57  - ( 1827.230272 ) );
    result.eq.push_back( - 2.08*x46 + x58  - ( 1827.230272 ) );
    result.eq.push_back( - 2.08*x47 + x59  - ( 1827.230272 ) );
    result.eq.push_back( - 4.18*log(0.00318642227232694*x46) + x60  );
    result.eq.push_back( - 4.18*log(0.00318642227232694*x47) + x61  );
    result.eq.push_back( - (2.08*log(0.00318642227232694*x46) - 0.462*log(100*x17)) + x62 - ( 7.90232723537082 ) );
    result.eq.push_back( - (2.08*log(0.00318642227232694*x47) - 0.462*log(100*x18)) + x63 - ( 7.90232723537082 ) );
    result.eq.push_back( - 4.18*x2 - 0.1*x8 + x23  - ( -1311.817088 ) );
    result.eq.push_back( - 4.18*x3 - 0.1*x11 + x26  - ( -1311.817088 ) );
    result.eq.push_back( - 4.18*x4 - 0.1*x12 + x27  - ( -1311.817088 ) );
    result.eq.push_back( - 2.08*x5 + x28  - ( 1827.230272 ) );
    result.eq.push_back( - 2.08*x6 + x29  - ( 1827.230272 ) );
    result.eq.push_back( - (2.08*log(0.00318642227232694*x6) - 0.462*log(100*x14)) + x34 - ( 7.90232723537082 ) );
    result.eq.push_back( - (x19*(x56 - x50) + x50) + x30  );
    result.eq.push_back( - (x20*(x57 - x51) + x51) + x31  );
    result.eq.push_back( - (x21*(x58 - x52) + x52) + x32  );
    result.eq.push_back( - (x22*(x59 - x53) + x53) + x33  );
    result.eq.push_back( - (x21*(x62 - x60) + x60) + x35  );
    result.eq.push_back( - (x22*(x63 - x61) + x61) + x36  );
    result.eq.push_back( - x34 + x35  );
    result.eq.push_back( - x9 + x17  );
    result.eq.push_back( - x9 + x15  );
    result.eq.push_back( - 0.9*x29 + 0.9*x32 + x74  );
    result.eq.push_back( - x29 + x30 + x74  );
    result.eq.push_back( - x64*x65*x74 + x75  );
    result.eq.push_back( - x34 + x36  );
    result.eq.push_back( - x8 + x18  );
    result.eq.push_back( - x8 + x16  );
    result.eq.push_back( - 0.9*x29 + 0.9*x33 + x76  );
    result.eq.push_back( - x29 + x31 + x76  );
    result.eq.push_back( - x64*(1 - x65)*x76 + x77  );
    result.eq.push_back( x7 - x45  );
    result.eq.push_back( x23 - x48  );
    result.eq.push_back( 0.125*x8 - 0.125*x9 + x70  );
    result.eq.push_back( - x23 + x24 - x70  );
    result.eq.push_back( - x64*(1 - x65)*x70 + x71  );
    result.eq.push_back( - x9 + x10  );
    result.eq.push_back( - (x65*x30 + (1 - x65)*x24) + x25  );
    result.eq.push_back( 0.125*x10 - 0.125*x11 + x72  );
    result.eq.push_back( - x25 + x26 - x72  );
    result.eq.push_back( - x64*x72 + x73  );
    result.eq.push_back( 200*x68 + x69  - ( 180000 ) );
    result.eq.push_back( - x11 + x12  );
    result.eq.push_back( - x12 + x13  );
    result.eq.push_back( - x13 + x14  );
    result.eq.push_back( x64*(x29 - x26) + 200*x68  - ( 180000 ) );
    result.eq.push_back( x64*(x29 - x28) + 200*x66  - ( 180000 ) );
    result.eq.push_back( x64*(x28 - x27) - 200*x66 + 200*x67  );
    result.eq.push_back( x28 - x54  );
    result.eq.push_back( x4 - x41  - ( -10 ) );
    result.eq.push_back( x71 + x73 - x75 - x77 + x78  );
    result.eq.push_back( x25 - x49  );
    result.eq.push_back( - x64*(1 - x65)*(x31 - x23) + x79  );
    result.eq.push_back( - x64*(x27 - x26) + x80  );
    result.eq.push_back( - x64*(x28 - x27) + x81  );
    result.eq.push_back( - x64*(x29 - x28) + x82  );
    result.eq.push_back( - 0.9*x64 + x111  );
    result.eq.push_back( - x7 + x83  - ( -303 ) );
    result.eq.push_back( - x2 + x87  - ( -298 ) );
    result.eq.push_back( x3 - x68 + x84  );
    result.eq.push_back( x4 - x67 + x88  );
    result.eq.push_back( x41 - x67 + x85  );
    result.eq.push_back( x41 - x66 + x89  );
    result.eq.push_back( x42 - x66 + x86  );
    result.eq.push_back( x6 + x90  - ( 900 ) );
    result.eq.push_back( - pow((0.5*x83*x87*(x83 + x87)),0.333333333333333) + x91  );
    result.eq.push_back( - pow((0.5*x84*x88*(x84 + x88)),0.333333333333333) + x92  );
    result.eq.push_back( - pow((0.5*x85*x89*(x85 + x89)),0.333333333333333) + x93  );
    result.eq.push_back( - pow((0.5*x86*x90*(x86 + x90)),0.333333333333333) + x94  );
    result.eq.push_back( - 2.85714285714286*x79/x91 + x95  );
    result.eq.push_back( - 16.6666666666667*x80/x92 + x96  );
    result.eq.push_back( - 16.6666666666667*x81/x93 + x97  );
    result.eq.push_back( - 33.3333333333333*x82/x94 + x98  );
    result.eq.push_back( - pow(10,(4.3247 + 0.1634*pow((log(x95)/log(10.)),2) - 0.303*(log(x95)/log(10.)))) + x99  );
    result.eq.push_back( - pow(10,(4.3247 + 0.1634*pow((log(x96)/log(10.)),2) - 0.303*(log(x96)/log(10.)))) + x100  );
    result.eq.push_back( - pow(10,(4.3247 + 0.1634*pow((log(x97)/log(10.)),2) - 0.303*(log(x97)/log(10.)))) + x101  );
    result.eq.push_back( - pow(10,(4.3247 + 0.1634*pow((log(x98)/log(10.)),2) - 0.303*(log(x98)/log(10.)))) + x102  );
    result.eq.push_back( - pow(10,(3.5565 + 0.3776*(log(x111)/log(10.)) + 0.0905*pow((log(x111)/log(10.)),2))) + x112 );
    result.eq.push_back( - pow(10,(0.03881 + 0.08183*pow((log(x11)/log(10.)),2) - 0.11272*(log(x11)/log(10.)))) + x104 );
    result.eq.push_back( - pow(10,(0.03881 + 0.08183*pow((log(x11)/log(10.)),2) - 0.11272*(log(x11)/log(10.)))) + x105 );
    result.eq.push_back( - pow(10,(0.03881 + 0.08183*pow((log(x11)/log(10.)),2) - 0.11272*(log(x11)/log(10.)))) + x106 );
    result.eq.push_back( x103  - ( 1 ) );
    result.eq.push_back( - (1.9234 + 5.3867*x103)*x99 + x107  );
    result.eq.push_back( - (1.9234 + 5.3867*x104)*x100 + x108  );
    result.eq.push_back( - (1.9234 + 5.3867*x105)*x101 + x109  );
    result.eq.push_back( - (1.9234 + 5.3867*x106)*x102 + x110  );
    result.eq.push_back( - 4.0002*x112 + x113  );
    result.eq.push_back( - 3540*pow(x71,0.71) + x114  );
    result.eq.push_back( - 3540*pow(x73,0.71) + x115  );
    result.eq.push_back( - (6000*pow((x75 + x77),0.7) + 60*pow((x75 + x77),0.95)) + x116  );
    result.eq.push_back( - x107 - x108 - x109 - x110 - x113 - x114 - x115 - x116 + x117  );
    result.eq.push_back( - x78 + x118  - ( 69676 ) );
    result.eq.push_back( - 0.25*(4515123 + 0.19875*x117)/x118 + x121  );
    result.eq.push_back( - 5.4836887677603E-6*x118 + x119  );
    result.eq.push_back( - 14/x119 + x120  );
    result.eq.push_back( x1 - x120 - x121  - ( 4 ) );

    // Relaxation only inequalities (<=0):

    // Relaxation only equalities (=0):

    return result;
}
