/**
* @file ../../output_MAiNGO_problem\problem_abel.h
*
* @brief This file was generated using the MAiNGOReaderWriter class.
*        File contains declaration of the Model object and the problem parsed from a GAMS convert file.
*
* ==============================================================================\n
* © Aachener Verfahrenstechnik-Systemverfahrenstechnik, RWTH Aachen University  \n
* ==============================================================================\n
*
* @author Dominik Bongartz, Jaromil Najman, Susanne Sass, Alexander Mitsos
* @date 6.11.2019
*
*/ 
#pragma once

#include "MAiNGOmodel.h"


using Var = mc::FFVar;    // This allows us to write Var instead of mc::FFVar


/**
* @class Model
* @brief Class defining the actual model implemented by the user 
*
* This class is used by the user to implement the model 
*/
class Model: public maingo::MAiNGOmodel {

    public:
        /**
        * @brief Default constructor 
        */
        Model();

        /**
        * @brief Main function used to evaluate the model and construct a directed acyclic graph
        *
        * @param[in] optVars is the optimization variables vector
        * @param[in] writeAdditionalOutput defines whether to write additional output 
        */  
        maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);

        /**
        * @brief Function for getting optimization variables data
        */
        std::vector<maingo::OptimizationVariable> get_variables();

        /**
        * @brief Function for getting initial point data
        */
        std::vector<double> get_initial_point();

    private:
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{

    std::vector<maingo::OptimizationVariable> variables;
    // Required: Define optimization variables by specifying lower bound, upper bound (, optionally variable type, branching priority and a name)
    // Continuous variables
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(387.9,387.9), maingo::VT_CONTINUOUS, "x1"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x2"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x3"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x4"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x5"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x6"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x7"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x8"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(85.3,85.3), maingo::VT_CONTINUOUS, "x9"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x10"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x11"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x12"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x13"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x14"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x15"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x16"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x17"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x18"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x19"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x20"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x21"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x22"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x23"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x24"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x25"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x26"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x27"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x28"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x29"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-10000000000,10000000000), maingo::VT_CONTINUOUS, "x30"));
    // Binary variables
    // Integer variables

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// function for providing initial point data to the Branch-and-Bound solver
std::vector<double>
Model::get_initial_point()
{

    // Here you can provide an initial point for the local search
    std::vector<double> initialPoint;
    // Continuous variables
    initialPoint.push_back(387.9);
    initialPoint.push_back(387.9);
    initialPoint.push_back(387.9);
    initialPoint.push_back(387.9);
    initialPoint.push_back(387.9);
    initialPoint.push_back(387.9);
    initialPoint.push_back(387.9);
    initialPoint.push_back(387.9);
    initialPoint.push_back(85.3);
    initialPoint.push_back(85.3);
    initialPoint.push_back(85.3);
    initialPoint.push_back(85.3);
    initialPoint.push_back(85.3);
    initialPoint.push_back(85.3);
    initialPoint.push_back(85.3);
    initialPoint.push_back(85.3);
    initialPoint.push_back(110.5);
    initialPoint.push_back(110.5);
    initialPoint.push_back(110.5);
    initialPoint.push_back(110.5);
    initialPoint.push_back(110.5);
    initialPoint.push_back(110.5);
    initialPoint.push_back(110.5);
    initialPoint.push_back(147.1);
    initialPoint.push_back(147.1);
    initialPoint.push_back(147.1);
    initialPoint.push_back(147.1);
    initialPoint.push_back(147.1);
    initialPoint.push_back(147.1);
    initialPoint.push_back(147.1);
    // Binary variables
    // Integer variables

    return initialPoint;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model()
{

    // Initialize data if necessary:

}


//////////////////////////////////////////////////////////////////////////
// Evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var> &optVars)
{

    maingo::EvaluationContainer result; /*!< Variable holding the actual result consisting of an objective, inequalities, equalities, relaxation only inequalities and relaxation only equalities */

    // Rename  inputs
    // Continuous variables
    unsigned int cVar = 0;
    Var x1 = optVars[cVar]; cVar++;
    Var x2 = optVars[cVar]; cVar++;
    Var x3 = optVars[cVar]; cVar++;
    Var x4 = optVars[cVar]; cVar++;
    Var x5 = optVars[cVar]; cVar++;
    Var x6 = optVars[cVar]; cVar++;
    Var x7 = optVars[cVar]; cVar++;
    Var x8 = optVars[cVar]; cVar++;
    Var x9 = optVars[cVar]; cVar++;
    Var x10 = optVars[cVar]; cVar++;
    Var x11 = optVars[cVar]; cVar++;
    Var x12 = optVars[cVar]; cVar++;
    Var x13 = optVars[cVar]; cVar++;
    Var x14 = optVars[cVar]; cVar++;
    Var x15 = optVars[cVar]; cVar++;
    Var x16 = optVars[cVar]; cVar++;
    Var x17 = optVars[cVar]; cVar++;
    Var x18 = optVars[cVar]; cVar++;
    Var x19 = optVars[cVar]; cVar++;
    Var x20 = optVars[cVar]; cVar++;
    Var x21 = optVars[cVar]; cVar++;
    Var x22 = optVars[cVar]; cVar++;
    Var x23 = optVars[cVar]; cVar++;
    Var x24 = optVars[cVar]; cVar++;
    Var x25 = optVars[cVar]; cVar++;
    Var x26 = optVars[cVar]; cVar++;
    Var x27 = optVars[cVar]; cVar++;
    Var x28 = optVars[cVar]; cVar++;
    Var x29 = optVars[cVar]; cVar++;
    Var x30 = optVars[cVar]; cVar++;
    // Binary variables
    unsigned int bVar = cVar;
    // Integer variables
    unsigned int iVar = bVar;

    // Objective function
    result.objective = -(-(0.5*((-24.24375 + 0.0625*x1)*(-387.9 + x1) + (-85.3 + x9)*(-85.3 + x9)+ (-24.425578125 + 0.0625*x2)*(-390.80925 + x2) + (-85.93975 + x10)
                       *(-85.93975 + x10) + (-24.6087699609375 + 0.0625*x3)*(-393.740319375 + x3) +(-86.584298125 + x11)*(-86.584298125 + x11) + (-24.7933357356445
                        + 0.0625*x4)*(-396.693371770313 + x4) + (-87.2336803609375 + x12)*(-87.2336803609375 + x12) + (-24.9792857536619 + 0.0625*x5)*(-399.66857205859
                        + x5) + (-87.8879329636445 + x13)*(-87.8879329636445 + x13) + (-25.1666303968143 + 0.0625*x6)*(-402.666086349029 + x6) + (-88.5470924608719
                        + x14)*(-88.5470924608719 + x14) + (-25.3553801247904 +0.0625*x7)*(-405.686081996647 + x7) + (-89.2111956543284 + x15)*(-89.2111956543284 
                       + x15) + (-2554.55454757264 + 6.25*x8)*(-408.728727611622+ x8) + (-8988.02796217359 + 100*x16)*(-89.8802796217359 + x16)) + 0.5*((-110.5 + x17)
                       *(-110.5 + x17) + (-65.3124 + 0.444*x24)*(-147.1 + x24) + (-111.32875 + x18)*(-111.32875 + x18) + (-65.802243 + 0.444*x25)*(-148.20325+ x25)
                        + (-112.163715625 + x19)*(-112.163715625 + x19) + (-66.2957598225+ 0.444*x26)*(-149.314774375 + x26) + (-113.004943492188 + x20)*(-113.004943492188
                        + x20) + (-66.7929780211688 + 0.444*x27)*(-150.434635182813 + x27) + (-113.852480568379 + x21)*(-113.852480568379 +x21) + (-67.2939253563275
                        + 0.444*x28)*(-151.562894946684 + x28) + (-114.706374172642 + x22)*(-114.706374172642 + x22) + (-67.7986297965 +0.444*x29)*(-152.699616658784
                        + x29) + (-115.566671978937 + x23)*(-115.566671978937 + x23) + (-68.3071195199738 + 0.444*x30)*(-153.844863783725 + x30))) );

    // Inequalities (<=0)

    // Equalities (=0)
    result.eq.push_back( - 0.914*x1 + x2 + 0.016*x9 - 0.305*x17 - 0.424*x24  - ( -59.4 ) );
    result.eq.push_back( - 0.914*x2 + x3 + 0.016*x10 - 0.305*x18 - 0.424*x25  - ( -59.4 ) );
    result.eq.push_back( - 0.914*x3 + x4 + 0.016*x11 - 0.305*x19 - 0.424*x26  - ( -59.4 ) );
    result.eq.push_back( - 0.914*x4 + x5 + 0.016*x12 - 0.305*x20 - 0.424*x27  - ( -59.4 ) );
    result.eq.push_back( - 0.914*x5 + x6 + 0.016*x13 - 0.305*x21 - 0.424*x28  - ( -59.4 ) );
    result.eq.push_back( - 0.914*x6 + x7 + 0.016*x14 - 0.305*x22 - 0.424*x29  - ( -59.4 ) );
    result.eq.push_back( - 0.914*x7 + x8 + 0.016*x15 - 0.305*x23 - 0.424*x30  - ( -59.4 ) );
    result.eq.push_back( - 0.097*x1 - 0.424*x9 + x10 + 0.101*x17 - 1.459*x24  - ( -184.7 ) );
    result.eq.push_back( - 0.097*x2 - 0.424*x10 + x11 + 0.101*x18 - 1.459*x25  - ( -184.7 ) );
    result.eq.push_back( - 0.097*x3 - 0.424*x11 + x12 + 0.101*x19 - 1.459*x26  - ( -184.7 ) );
    result.eq.push_back( - 0.097*x4 - 0.424*x12 + x13 + 0.101*x20 - 1.459*x27  - ( -184.7 ) );
    result.eq.push_back( - 0.097*x5 - 0.424*x13 + x14 + 0.101*x21 - 1.459*x28  - ( -184.7 ) );
    result.eq.push_back( - 0.097*x6 - 0.424*x14 + x15 + 0.101*x22 - 1.459*x29  - ( -184.7 ) );
    result.eq.push_back( - 0.097*x7 - 0.424*x15 + x16 + 0.101*x23 - 1.459*x30  - ( -184.7 ) );

    // Relaxation only inequalities (<=0):

    // Relaxation only equalities (=0):

    return result;
}
