/**
* @file ../../output_MAiNGO_problem\problem_alkyl.h
*
* @brief This file was generated using the MAiNGOReaderWriter class.
*        File contains declaration of the Model object and the problem parsed from a GAMS convert file.
*
* ==============================================================================\n
* © Aachener Verfahrenstechnik-Systemverfahrenstechnik, RWTH Aachen University  \n
* ==============================================================================\n
*
* @author Dominik Bongartz, Jaromil Najman, Susanne Sass, Alexander Mitsos
* @date 6.11.2019
*
*/ 
#pragma once

#include "MAiNGOmodel.h"


using Var = mc::FFVar;    // This allows us to write Var instead of mc::FFVar


/**
* @class Model
* @brief Class defining the actual model implemented by the user 
*
* This class is used by the user to implement the model 
*/
class Model: public maingo::MAiNGOmodel {

    public:
        /**
        * @brief Default constructor 
        */
        Model();

        /**
        * @brief Main function used to evaluate the model and construct a directed acyclic graph
        *
        * @param[in] optVars is the optimization variables vector
        * @param[in] writeAdditionalOutput defines whether to write additional output 
        */  
        maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);

        /**
        * @brief Function for getting optimization variables data
        */
        std::vector<maingo::OptimizationVariable> get_variables();

        /**
        * @brief Function for getting initial point data
        */
        std::vector<double> get_initial_point();

    private:
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{

    std::vector<maingo::OptimizationVariable> variables;
    // Required: Define optimization variables by specifying lower bound, upper bound (, optionally variable type, branching priority and a name)
    // Continuous variables
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,2), maingo::VT_CONTINUOUS, "x2"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1.6), maingo::VT_CONTINUOUS, "x3"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1.2), maingo::VT_CONTINUOUS, "x4"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5), maingo::VT_CONTINUOUS, "x5"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,2), maingo::VT_CONTINUOUS, "x6"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.85,0.93), maingo::VT_CONTINUOUS, "x7"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.9,0.95), maingo::VT_CONTINUOUS, "x8"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(3,12), maingo::VT_CONTINUOUS, "x9"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1.2,4), maingo::VT_CONTINUOUS, "x10"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1.45,1.62), maingo::VT_CONTINUOUS, "x11"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.99,1.01010101010101), maingo::VT_CONTINUOUS, "x12"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.99,1.01010101010101), maingo::VT_CONTINUOUS, "x13"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.9,1.11111111111111), maingo::VT_CONTINUOUS, "x14"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.99,1.01010101010101), maingo::VT_CONTINUOUS, "x15"));
    // Binary variables
    // Integer variables

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// function for providing initial point data to the Branch-and-Bound solver
std::vector<double>
Model::get_initial_point()
{

    // Here you can provide an initial point for the local search
    std::vector<double> initialPoint;
    // Continuous variables
    initialPoint.push_back(1.745);
    initialPoint.push_back(1.2);
    initialPoint.push_back(1.1);
    initialPoint.push_back(3.048);
    initialPoint.push_back(1.974);
    initialPoint.push_back(0.893);
    initialPoint.push_back(0.928);
    initialPoint.push_back(8);
    initialPoint.push_back(3.6);
    initialPoint.push_back(1.45);
    initialPoint.push_back(1);
    initialPoint.push_back(1);
    initialPoint.push_back(1);
    initialPoint.push_back(1);
    // Binary variables
    // Integer variables

    return initialPoint;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model()
{

    // Initialize data if necessary:

}


//////////////////////////////////////////////////////////////////////////
// Evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var> &optVars)
{

    maingo::EvaluationContainer result; /*!< Variable holding the actual result consisting of an objective, inequalities, equalities, relaxation only inequalities and relaxation only equalities */

    // Rename  inputs
    // Continuous variables
    unsigned int cVar = 0;
    Var x2 = optVars[cVar]; cVar++;
    Var x3 = optVars[cVar]; cVar++;
    Var x4 = optVars[cVar]; cVar++;
    Var x5 = optVars[cVar]; cVar++;
    Var x6 = optVars[cVar]; cVar++;
    Var x7 = optVars[cVar]; cVar++;
    Var x8 = optVars[cVar]; cVar++;
    Var x9 = optVars[cVar]; cVar++;
    Var x10 = optVars[cVar]; cVar++;
    Var x11 = optVars[cVar]; cVar++;
    Var x12 = optVars[cVar]; cVar++;
    Var x13 = optVars[cVar]; cVar++;
    Var x14 = optVars[cVar]; cVar++;
    Var x15 = optVars[cVar]; cVar++;
    // Binary variables
    unsigned int bVar = cVar;
    // Integer variables
    unsigned int iVar = bVar;

    // Objective function
    result.objective = -(6.3*x5*x8 - 5.04*x2 - 0.35*x3 - x4 - 3.36*x6 );

    // Inequalities (<=0)

    // Equalities (=0)
    result.eq.push_back( - 0.819672131147541*x2 + x5 - 0.819672131147541*x6  );
    result.eq.push_back( 0.98*x4 - (0.01*x5*x10 + x4)*x7  );
    result.eq.push_back( -x2*x9 + 10*x3 + x6  );
    result.eq.push_back( x5*x12 - (1.12 + 0.13167*x9 - 0.0067*x9*x9)*x2  );
    result.eq.push_back( x8*x13 - 0.01*(1.098*x9 - 0.038*x9*x9) - 0.325*x7  - ( 0.57425 ) );
    result.eq.push_back( x10*x14 + 22.2*x11  - ( 35.82 ) );
    result.eq.push_back( x11*x15 - 3*x8  - ( -1.33 ) );

    // Relaxation only inequalities (<=0):

    // Relaxation only equalities (=0):

    return result;
}
