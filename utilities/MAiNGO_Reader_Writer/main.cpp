/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include "MAiNGOReaderWriter.h"

/**
* @brief Main function for reading GAMS convert files and writing MAiNGO problem
 *        files out of it. It can also read MAiNGO log files.
*
*/
int
main(int argc, char *argv[])
{

    // Read GAMS file name
    std::string gamsFile = "../../example/gams.gms";    // You most likely need to adapt this path to your folder structure or specify it directly as a command line argument
    std::string dictFile = "../../example/dict.txt";
	if (argc >= 2) {
        gamsFile = argv[1];
        if (argc >= 3) {
            dictFile = argv[2];
            if (argc > 3) {
                std::cout << "Warning: Accept only the GAMS file and dict file name as input. Ignoring additional command line arguments." << std::endl
                          << std::endl;
            }
        }
    }

	try {
		// Create MAiNGOReaderWriter object
		maingo::readerWriter::MAiNGOReaderWriter maingoRW;

		// Read GAMS file
		maingoRW.read_GAMS_convert_file(gamsFile);

		// Print problem information
		// maingoRW.print_problem_info_GAMS_file(std::cout);

		// Write MAiNGO problem file, default name is problem_gams.h
		maingoRW.write_MAiNGO_problem_file("problem_gams", dictFile);

		// Write ALE problem file, default name is problem_gams.txt
		maingoRW.write_ALE_problem_file("problem_gams", dictFile);

		// Read MAiNGO log file
		// maingoRW.read_MAiNGO_log_file("../../example/bab.log");

		// Print info read in from log file
		// maingoRW.print_problem_info_MAiNGO_log(std::cout);

		// Convert all GAMS files found in folder "input GAMS convert" and write them into the "output MAiNGO problem" folder
		// maingoRW.convert_GAMS_folder_to_MAiNGO_problem("../../input_GAMS_convert", "../../output_MAiNGO_problem");

		// Convert all GAMS files found in folder "input GAMS convert" and write them into the "output ALE problem" folder
		// maingoRW.convert_GAMS_folder_to_ALE_problem("../../input_GAMS_convert", "../../output_ALE_problem");
	}
	catch (const std::exception& e) {
		std::cout << "Encountered a fatal error:" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}
	catch (...) {
		std::cout << "Encountered an unknown fatal error." << std::endl;
		return -1;
	}

    return 0;
}