# MAiNGO Reader Writer Utility

This is a utility that contains multiple useful functions for working with MAiNGO.
For example, it can convert a problem in GAMS convert format to the text-based input format of MAiNGO, or input for its C++ API.
For more information, refer to the documentation of the MAiNGO Reader Writer at `utilities/MAiNGO_Reader_Writer/doc/html/index.html`