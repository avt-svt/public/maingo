
close all
clear all

M=csvread('MAiNGO_epsilon_constraint_objective_values.csv',1,0);

plot(M(:,1),M(:,2),'bo');


%----------------------Plot-Layout-------------------------------------------------
title('Pareto Front') %Title of the plot
axis square;
xlabel('Obj_1'); %Label of the x-axis
ylabel('Obj_2'); %Label of the y-axis
Figure_Format_ppt %Sets the Layout for axis, legend, background
grid off;
%----------------------------------------------------------------------------------


%----------------------Plot-Saving-------------------------------------------------
Resolution = '-r300' ;
Filename = '2D-Pareto-Front'; 

% Format = '-dmeta'            ; % enhanced metafile
% Format = '-depsc'             ; % Encapsulated PostScript (EPS) Level 3 color
% Format = '-dpdf' ;
Format = '-dpng' ;
print(Filename,Format,Resolution )
%----------------------------------------------------------------------------------