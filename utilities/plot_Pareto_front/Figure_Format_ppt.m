% Figures formatieren, so dass sie direkt in Berichten verwendet
% werden kann

% Zu den Anpassungen geh�ren:
% Gr��e der Figure anpassen
% Hintergrundfarbe weiss
% Grid einschalten
% Beschriftungen Arial mit Gr��e 16
% Dicke der Linien 3.0
% Dicke der Axen 0.85
% auskommentiert: Anpassen der colorbar

% (c)Sebastian Recker 2010
% jume: F�r mich angepasst

% Attention: Most of the code has been developed for the old Matlab
% graphics system. Nowdays there are much more elegant ways to handle and
% modify Matlab figures. https://de.mathworks.com/products/matlab/matlab-graphics.html


for i = 1 : 1%gcf   %iteriert �ber Anzahl der Figures
    
    hfig=figure(i);
    hcfig = get(hfig,'Children');
    anzahl_subplots = length(hcfig);
    
    
    % Gr��e der Figure in cm festlegen
     set(gcf,'PaperUnits','centimeters','PaperPosition',[10 5 20 15]);
     set(gcf,'Units','centimeters','position',get(gcf,'PaperPosition'))

    
    
    hcax = get(gcf,'Children');
    anzahl_axes = length(hcax);
    
    % Grid einschalten
    grid on;
    
    % Hintergrundfarben wei�
    set(gcf,'Color','white');
    set(gca,'Color','white');
    
    % Beschriftung auf Arial und Gr��e 16
    set(gca,'FontName','Arial','FontSize',12);
    set(get(gca,'XLabel'),'FontName','Arial','FontSize',12);
    set(get(gca,'YLabel'),'FontName','Arial','FontSize',12);
    set(get(gca,'ZLabel'),'FontName','Arial','FontSize',12);
    set(get(gca,'Title'),'FontName','Arial','FontSize',12);
    
    % Dicke der Achsenlinien auf 0.85
    set(gca,'LineWidth',0.85);
    
    % Liniendicke auf 3.0
    all_lines = findobj(gca,'Type','line');
%     set(all_lines,'LineWidth',1.5);
    
    % Legende formatieren
    [legend_h,object_h,plot_h,text_strings] = legend;
    set(legend_h,'FontName','Arial','FontSize',10);

%     set(legend_h,'FontName','Arial','FontSize',10,'Location','Best');
    
    %% Falls colorbar in plot
    % cbar_h = colorbar;
    % set(cbar_h,'FontName','Arial');
    % set(cbar_h,'FontSize',12);
    
    % Anpasssen der Colormap
    % CMgray = gray;
    % set(gcf,'Colormap',CMgray);
    % end
    %end
    
    % Damit die Achsenbeschriftungen auch bei kleineren Grafiken noch angezeigt
    % werden
%     pos = get(gca,'position'); % This is in normalized coordinates
%     pos(3:4)=pos(3:4)*.95; % Shrink the axis by a factor of .9
%     pos(1:2)=pos(1:2)+pos(3:4)*.04; % Center it in the figure window

%     set(gca,'position',pos);
    
    
end

